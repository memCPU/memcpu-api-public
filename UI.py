import os
import json
import re
import glob
import copy
import sys
import pandas as pd
import tkinter as tk
from tkinter import filedialog
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import PySimpleGUI as sg

from memAPI.configjson import SmartConfig
import memAPI.versions as vs
import memAPI.misc as ms
import memAPI.plotters as pt
import memAPI.core as core

# TODO for all UIs, create dictionary of ui values to check/update plots, instead of the individual logic currently
########################################

sg.theme('Reddit')
# root = tk.Tk()
# root.withdraw()

########################################

default_UI_settings = {
	'timeout': [1, float('inf')],
	'mode': [0, 2],
	'cores': [1, 36],
	'simtime': [0, float('inf')],
	'cputime': [0, float('inf')],
	'replicas': [1, float('inf')],
	'initialobjective': [float('-inf'), float('inf')],
	'targetobjective': [float('-inf'), float('inf')],
	'feasibilitytolerance': [0, float('inf')],
	'iters': [1, float('inf')],
	'mchains': [2, float('inf')],
	'sigmamin': [0, 1],
	'sigmamax': [0, 1],
	'switchfraction': [0, 1],
	'warmstart': '',
	'stride': [0, 100],
	'fixedseed': [1, float('inf')],
	'itersdelaythermalx': [0, float('inf')],
	'nsetsfrompartabledss': [1, float('inf')],
	'unaryresbase': [1, float('inf')],
	'maxexpansionfactor': [1, 10000]
}

default_config_settings = {
	'name': '',
	'timeout': [],
	'cores': 36,
	'simtime': 8,
	'cputime': 3600,
	'initialobjective': 'inf',
	'targetobjective': '-inf',
	'feasibilitytolerance': 1e-6,
	'switchfraction': 0.01,
	'sigmamin': 0.01,
	'sigmamax': 0.25,
	'iters': 1000,
	'mchains': 36,
	'replicas': 2,
	'warmstart': None,
	'repeats': 8,
	'stride': 50
}


def get_mps_ui():
	# File Dialog Prompt #
	if sys.platform in ['darwin', 'linux']:
		filetypes = ()
	else:
		filetypes = [("MPS problem file", "*.mps *.mps.gz")]
	file_path = tk.filedialog.askopenfilename(filetypes=filetypes)

	return file_path


def get_json_ui():
	# File Dialog Prompt #
	if sys.platform in ['darwin', 'linux']:
		filetypes = ()
	else:
		filetypes = [("JSON", "*.json")]
	file_path = tk.filedialog.askopenfilename(filetypes=filetypes)

	return file_path


def get_history_ui(standalone=False):
	"""
	Prompts the User for a .mps_history file
	:return: tuple: (file path, version number)
	"""
	if not standalone:
		# File Dialog Prompt #
		if sys.platform in ['darwin', 'linux']:
			filetypes = ()
		else:
			filetypes = [("mps history files", ("*history", ".mps_history", ".mps.gz_history"))]
		file_path = tk.filedialog.askopenfilename(
			filetypes=filetypes)
		# file_name = os.path.basename(file_path)
		# file_name = os.path.splitext(file_name)[0]
		file_path, version = ms.get_history(file_path)
	else:
		# File Dialog Prompt #
		folder_path = tk.filedialog.askdirectory()
		# file_name = os.path.basename(file_path)
		# file_name = os.path.splitext(file_name)[0]
		file_path = []
		version = []
		if not folder_path:
			return None, None
		for file in glob.glob(folder_path + '/*history*'):
			file_p, vers = ms.get_history(file)
			file_path.append(file_p)
			version.append(vers)

	return file_path, version


def get_credentials_ui():
	if os.path.isfile('settings.json'):
		with open('settings.json') as json_file:
			settings = json.load(json_file)
	else:
		settings = {
			"username": "",
			"password": "",
			"api-key": "",
			"proxy": {
				"http": "",
				"https": ""
			}
		}

	form = sg.FlexForm('Login')  # begin with a blank form

	layout = [
		[sg.Text('SaaS API Login Credentials', font=('default', 14, 'bold'))],
		[sg.Text('MemComputing Inc. ')],
		[sg.Text('_' * 60)],
		[sg.Text('Username:', size=(15, 1)), sg.In(default_text=settings['username'], key='username')],
		[sg.Text('Password:', size=(15, 1)),
		 sg.In(default_text=settings['password'], password_char='*', key='password')],
		[sg.Text('API Key:', size=(15, 1)), sg.In(default_text=settings['api_key'], key='api_key')],
		[sg.Checkbox('Remember Login', key='save_login')],
		[sg.Text('Optional Proxy Server Settings:')],
		[sg.Text('http:', size=(15, 1)), sg.In(default_text='', key='http')],
		[sg.Text('https:', size=(15, 1)), sg.In(default_text='', key='https')],

		[sg.Submit()]
	]
	form.Layout(layout)
	while True:
		button, credentials = form.Read()
		if button is None:
			break
		if button == 'Submit':
			if not credentials['username']:
				sg.Popup('Missing:', 'Username')
			elif not credentials['password']:
				sg.Popup('Missing:', 'Password')
			elif not credentials['api_key']:
				sg.Popup('Missing:', 'API Key')
			else:
				settings['username'] = credentials['username']
				settings['password'] = credentials['password']
				settings['api_key'] = credentials['api_key']
				settings['proxy']['http'] = credentials['http']
				settings['proxy']['https'] = credentials['https']

				r1 = core.auth_test((settings['username'], settings['password']), settings['proxy'])
				if r1.status_code == 200:
					# Set Token
					res = r1.json()
					core.token = res['access']
					if credentials['save_login']:  # TODO save new credentials to settings.json file
						with open('settings.json', 'w') as outfile:
							json.dump(settings, outfile, indent=4)
					break
				elif r1.status_code == 401:
					sg.Popup('Login Failed:', 'Username and/or password is incorrect')
				else:
					sg.Popup('Login Failed:', 'Error ' + str(r1.status_code))

	form.Close()

	return settings


class ExtractPartableUI:
	def __init__(self, api):
		self.api = api
		history_file = self.pick_history()
		print(history_file)
		dataframe = ms.create_dataframe(*ms.get_history(history_file))
		self.extract_partable_UI(dataframe=dataframe)

	def pick_history(self):
		layout = [
			[sg.Text('MemComputing Inc. Plotting Settings', size=(42, 1))],
			[sg.Button('Open'), sg.Button('Download')]
		]

		form = sg.FlexForm('Extract Partable', layout)
		while True:

			button, values = form.Read(timeout=500)
			if button == 'Close' or button is None:
				break
			elif button == 'Download':
				history_file = self.get_dps_API()
				return history_file
			elif button == 'Open':
				history_file = get_history_ui()
				return history_file[0]


	def get_dps_API(self):
		if self.api is None:
			self.api = core.MemSaaS()
		if not self.api.has_credentials():
			self.api.get_credentials_ui()
		if not self.api.has_credentials():
			return

		response = self.api.list_my_jobs()
		print(response)
		if response:
			job_result = response.json()
			job_result_filtered = []
			counter = 0
			for num, job in enumerate(job_result):
				# Filter For DPS Jobs
				if job['solver'] == 'mc':
					job_result_filtered.append(job)
					if 'status' in job:
						job_result_filtered[counter]['status'] = job_result_filtered[counter]['status']['name']
					counter += 1

			job_dataframe = pd.DataFrame.from_dict(job_result_filtered)
		else:
			job_dataframe = pd.DataFrame(columns=['id', 'name', 'status', 'completed_at'])

		job_dataframe.sort_values(by=['id'], inplace=True, ascending=False)

		job_id = None
		folder_path = self.api.default_download_folder
		layout = [
			[sg.Table(values=job_dataframe[['id', 'name', 'status', 'completed_at']].values.tolist(), key='job',
			          headings=['ID', 'Name', 'Status', 'Completed'],
			          auto_size_columns=False, col_widths=[4, 16, 8, 12],
			          def_col_width=6, max_col_width=20, justification='left', num_rows=15)],
			[sg.Button('Select')]
		]

		form = sg.FlexForm('Job History', layout)
		while True:

			button, values = form.Read(timeout=500)
			if button == 'Close' or button is None:
				break
			elif button == 'Select':
				if values['job']:
					job_id = job_dataframe.iloc[values['job'][0]]['id']
					response_file_list = self.api.list_files(job_id)
					try:
						files_result = response_file_list.json()
					except:
						sg.Popup('Error:', 'Failed to Retrieve Job Files')
						job_id = None
						continue
					file_name = None
					for file_dict in files_result:
						if file_dict['name'].endswith('_history'):
							file_name = file_dict['name']
							break
					if file_name:
						self.api.get_named_file(job_id, file_name, file_destination=folder_path)
						return os.path.join(folder_path, file_name)

				else:
					sg.Popup('Warning:', 'No Job Selected')

	def extract_partable_UI(self, dataframe):
		pass


def plotting_ui(dataframe, visibility=False):
	form = sg.Window('Plotter')
	cleaned_dataframe = ms.sort_values(ms.clean_dataframe(dataframe.copy()), ['S', 'R', 'T'])
	parameter_list = ms.get_parameters(dataframe=dataframe)
	col_2 = [
		[sg.Checkbox('Set Soft Bounds:', size=(13, 1), key='use_soft', default=True),
		 sg.Combo(values=['Best Points', 'By Parameter Set', 'Objective Threshold', 'Distance Threshold'], default_value='Best Points',
		          key='bounds_plot_type_soft')],
		[sg.Text('Data Points:             '), sg.In(default_text='10', key='soft', size=(16, 1))],
		[sg.Text('Threshold:                '), sg.In(default_text='0', key='soft_threshold', size=(16, 1))],
		[sg.Checkbox('Set Hard Bounds:', size=(13, 1), key='use_hard', default=True),
		 sg.Combo(values=['Best Points', 'By Parameter Set', 'Objective Threshold', 'Distance Threshold'], default_value='Best Points',
		          key='bounds_plot_type_hard')],
		[sg.Text('Data Points:             '), sg.In(default_text='100', key='hard', size=(16, 1))],
		[sg.Text('Threshold:                '), sg.In(default_text='0', key='hard_threshold', size=(16, 1))],
		[sg.Text('Soft Bound Offset:     '), sg.In(default_text='0', key='soft_offset', size=(16, 1))],
		[sg.Text('Hard Bound Offset:    '), sg.In(default_text='0', key='hard_offset', size=(16, 1))],
		[sg.Text(u"\u203E" * 198, size=(38, 1))],
		[sg.Checkbox('Override Bound: ', size=(13, 1), key='override_bound', default=False), sg.Combo(
			values=parameter_list, key='override_param', size=(16, 1))],
		[sg.Text('', size=(6, 1)), sg.Text('Lower Bounds'), sg.Text('Upper Bounds')],
		[sg.Text('Soft: ', size=(6, 1)), sg.InputText(size=(12, 1), key='softlb'),
		 sg.InputText(size=(12, 1), key='softub')],
		[sg.Text('Hard: ', size=(6, 1)), sg.InputText(size=(12, 1), key='hardlb'),
		 sg.InputText(size=(12, 1), key='hardub')],
		[sg.Checkbox('Tune: ', size=(9, 1), key='tune', default=True), sg.Text('Default'), sg.In(size=(12, 1), key='default')],
		[sg.Text(u"\u203E" * 198, size=(38, 1))],
		[sg.Text('Or Select By Parameter Set(s) to bound: ', size=(30, 1))],
		[sg.Combo(values=['soft_bounds', 'hard_bounds'], default_value='soft_bounds', key='which_bounds'),
		 sg.Button('Export Selected Parameter Sets')],
		[sg.Checkbox('Best Selected Plot', key='parset_plot')],
		[sg.Table(values=[[i, *x] for i, x in enumerate(cleaned_dataframe[['S', 'R', 'T']].values.tolist())],
		          key='parset',
		          headings=['Set', 'Distance', 'Objective', 'Stability'],
		          alternating_row_color='lightblue', auto_size_columns=True, def_col_width=2,
		          max_col_width=18)]
	]

	col_1 = [
		[sg.Checkbox('O/C (Objective/Constraint) Plot', key='sr_plot'),
		 sg.Checkbox('Histogram Plot', key='histogram_plot')],
		[sg.Checkbox('Toggle: Unsatisfied (Constraints/Distance)', key='unsat_dist')],
		[sg.Text('Bins:', size=(22, 1)), sg.In(default_text=25, key='bins', size=(16, 1))],
		[sg.Text('Histogram Data Points:', size=(22, 1)), sg.In(default_text=200, key='data_points', size=(16, 1))],
		[sg.Combo(values=['Objective', 'Constraints'], default_value='Objective', key='which_y_scale'), sg.Combo(values=['linear', 'symlog'], default_value='linear', key='y_scale')],
		[sg.Text('(Symlog may modify min/max limits to display scale)')],
		[sg.Text('Objective-Offset:', size=(22, 1)), sg.In(default_text=0, key='y_offset', size=(16, 1))],
		[sg.Checkbox('Set Objective Max:', key='use_y_max', size=(19, 1)), sg.In(key='y_max', size=(16, 1))],
		[sg.Checkbox('Set Objective Min:', key='use_y_min', size=(19, 1)), sg.In(key='y_min', size=(16, 1))],
		[sg.Text(u"\u203E" * 198, size=(38, 1))],
		[sg.Checkbox('Best Set Plot', key='vlines_plot')],
		[sg.Text('Best Data Points', size=(22, 1)), sg.In(default_text=10, key='vlines', size=(16, 1))],
		[sg.Text(u"\u203E" * 198, size=(38, 1))],
		[sg.Button('Export Best Parameter Sets'), sg.Button('Export Bounds JSON')],
		[sg.Button('Import Bounds/Config JSON')],
		[sg.Button('Submit Bounds', visible=visibility)],
		[sg.Text(u"\u203E" * 198, size=(38, 1))],
		[sg.Button('Refresh Plot'), sg.Text(' ' * 50, size=(20, 1)), sg.Button('Close')]
	]

	layout = [
		[sg.Text('MemComputing Inc. Plotting Settings', size=(42, 1)), sg.Checkbox('Set Bounds', key='bounds_plot')],
		[sg.Text(u"\u203E" * 198, size=(42, 1)), sg.Text(u"\u203E" * 198, size=(42, 1))],
		[sg.Column(col_1), sg.Column(col_2, key='bounds_column', visible=False)]
	]

	form.Layout(layout)  # begin with a blank form
	bounds = None
	bounds_final = None
	bounds_override = copy.deepcopy(vs.default_bounds)
	for index in range(len(bounds_override)):
		bounds_override[index]['override'] = 0
	fig_handle = pt.full_plot(dataframe, control=None, bounds=None, fig=None)
	use_soft = True
	soft_bound_points = None
	soft_bound_parsets = []
	use_hard = True
	soft_bounds_offset = 0
	hard_bounds_offset = 0
	hard_bound_points = None
	hard_bound_parsets = []
	last = 'soft_bounds'
	last_param = None
	last_override = False
	last_soft_type = None
	last_hard_type = None
	update_plot = False
	custom_bounds = False
	update_bounds_element = False
	parsets = []
	while True:
		button, values = form.Read(timeout=500)
		if button == 'Close' or button is None:
			break

		if values['bounds_plot']:
			form.Element('bounds_column').Update(visible=True)
		else:
			form.Element('bounds_column').Update(visible=False)

		# Verify input fields are valid ints
		values['bins'] = int(values['bins']) if ms.type_coercion(values['bins'], int) and int(
			values['bins']) != 0 else 1
		values['data_points'] = int(values['data_points']) if ms.type_coercion(values['data_points'], int) else 1
		values['vlines'] = int(values['vlines']) if ms.type_coercion(values['vlines'], int) else 0
		values['soft'] = int(values['soft']) if ms.type_coercion(values['soft'], int) else 0
		values['hard'] = int(values['hard']) if ms.type_coercion(values['hard'], int) else 0

		# Verify input is float
		values['soft_offset'] = float(values['soft_offset']) if ms.type_coercion(values['soft_offset'], float) else 0
		values['hard_offset'] = float(values['hard_offset']) if ms.type_coercion(values['hard_offset'], float) else 0
		values['y_offset'] = float(values['y_offset']) if ms.type_coercion(values['y_offset'], float) else 0
		values['y_min'] = float(values['y_min']) if ms.type_coercion(values['y_min'], float) else 0
		values['y_max'] = float(values['y_max']) if ms.type_coercion(values['y_max'], float) else 1
		values['soft_threshold'] = int(values['soft_threshold']) if ms.type_coercion(values['soft_threshold'], float) else 0
		values['hard_threshold'] = int(values['hard_threshold']) if ms.type_coercion(values['hard_threshold'], float) else 0

		# change bool to int
		values['tune'] = int(values['tune'])

		if values['bounds_plot']:
			# Update bound points
			if soft_bound_points != values['soft']:
				update_plot = True
				soft_bound_points = values['soft']
			if use_soft != values['use_soft']:
				update_plot = True
				use_soft = values['use_soft']
			if hard_bound_points != values['hard']:
				update_plot = True
				hard_bound_points = values['hard']
			if use_hard != values['use_hard']:
				update_plot = True
				use_hard = values['use_hard']
			if soft_bounds_offset != values['soft_offset']:
				update_plot = True
				soft_bounds_offset = values['soft_offset']
			if hard_bounds_offset != values['hard_offset']:
				update_plot = True
				hard_bounds_offset = values['hard_offset']

			# Update UI element to keep history of last selected
			if values['which_bounds'] != last:
				last = values['which_bounds']
				if values['which_bounds'] == 'soft_bounds':
					if isinstance(soft_bound_parsets, int):
						form.Element('parset').Update(select_rows=[soft_bound_parsets])
					else:
						form.Element('parset').Update(select_rows=soft_bound_parsets)
				elif values['which_bounds'] == 'hard_bounds':
					if isinstance(hard_bound_parsets, int):
						form.Element('parset').Update(select_rows=[hard_bound_parsets])
					else:
						form.Element('parset').Update(select_rows=hard_bound_parsets)

			# Update bound input variables
			else:
				if values['which_bounds'] == 'soft_bounds':
					if soft_bound_parsets != values['parset']:
						if values['bounds_plot_type_soft'] == 'By Parameter Set' or values['bounds_plot_type_hard'] == 'By Parameter Set':
							update_plot = True
						soft_bound_parsets = values['parset']

				elif values['which_bounds'] == 'hard_bounds':
					if hard_bound_parsets != values['parset']:
						if values['bounds_plot_type_soft'] == 'By Parameter Set' or values['bounds_plot_type_hard'] == 'By Parameter Set':
							update_plot = True
						hard_bound_parsets = values['parset']

			if parsets != values['parset']:
				update_plot = True
				parsets = values['parset']

			# Set inputs to bounds function
			soft_input = None
			hard_input = None
			# Check if type of bound has been changed
			if last_soft_type != values['bounds_plot_type_soft']:
				update_plot = True
				last_soft_type = values['bounds_plot_type_soft']

			if last_hard_type != values['bounds_plot_type_hard']:
				update_plot = True
				last_hard_type = values['bounds_plot_type_hard']

			if values['bounds_plot_type_soft'] == 'Best Points':
				soft_input = soft_bound_points
			elif values['bounds_plot_type_soft'] == 'By Parameter Set':
				soft_input = soft_bound_parsets
			# Precalculate number of points for threshold bounding, use cleaned dataframe
			elif values['bounds_plot_type_soft'] == 'Objective Threshold':
				soft_input = len(cleaned_dataframe[cleaned_dataframe['R'] <= values['soft_threshold']])
			elif values['bounds_plot_type_soft'] == 'Distance Threshold':
				soft_input = len(cleaned_dataframe[cleaned_dataframe['S'] <= values['soft_threshold']])

			if values['bounds_plot_type_hard'] == 'Best Points':
				hard_input = hard_bound_points
			elif values['bounds_plot_type_hard'] == 'By Parameter Set':
				hard_input = hard_bound_parsets
			# Precalculate number of points for threshold bounding, use cleaned dataframe
			elif values['bounds_plot_type_hard'] == 'Objective Threshold':
				hard_input = len(cleaned_dataframe[cleaned_dataframe['R'] <= values['hard_threshold']])
			elif values['bounds_plot_type_hard'] == 'Distance Threshold':
				hard_input = len(cleaned_dataframe[cleaned_dataframe['S'] <= values['hard_threshold']])

			if update_plot:
				if custom_bounds:
					custom_bounds = False
				else:
					bounds = ms.get_new_bounds(dataframe, soft=soft_input, hard=hard_input, offset=(soft_bounds_offset, hard_bounds_offset),
				                           use_soft=values['use_soft'], use_hard=values['use_hard'])

			for bnd in ['hardlb', 'hardub', 'softlb', 'softub']:
				for i in range(len(parameter_list)):
					if not bounds_override[i]['override']:
						bounds_override[i][bnd] = bounds[i][bnd]

			if last_param != values['override_param']:
				last_param = values['override_param']
				update_bounds_element = True
			else:
				if last_param:
					if last_override != values['override_bound']:
						last_override = values['override_bound']
						update_bounds_element = True
						update_plot = True
					index = parameter_list.index(last_param)
					bounds_override[index]['override'] = values['override_bound']
					if bounds_override[index]['tune'] != values['tune']:
						bounds_override[index]['tune'] = values['tune']
						bounds_override[index]['tune'] = values['tune']
					if ms.type_coercion(values['default'], ty=float):
						if bounds_override[index]['default'] != [float(values['default'])]:
							update_plot = True
							bounds_override[index]['default'] = [float(values['default'])]
					for bnd in ['hardlb', 'hardub', 'softlb', 'softub']:
						if bounds_override[index]['override']:
							if ms.type_coercion(values[bnd], ty=float):
								if bounds_override[index][bnd] != float(values[bnd]):
									update_plot = True
									bounds_override[index][bnd] = float(values[bnd])
					if bounds_override[index]['hardlb'] > bounds_override[index]['softlb']:
						update_bounds_element = True
						bounds_override[index]['hardlb'] = bounds_override[index]['softlb']
					if bounds_override[index]['hardub'] < bounds_override[index]['softub']:
						update_bounds_element = True
						bounds_override[index]['hardub'] = bounds_override[index]['softub']

			if update_bounds_element:
				update_bounds_element = False
				if last_param:
					index = parameter_list.index(last_param)
					form.Element('override_bound').Update(value=bounds_override[index]['override'])
					print(bounds_override[index]['default'])
					form.Element('default').Update(value=bounds_override[index]['default'])
					form.Element('tune').Update(value=bounds_override[index]['tune'])
					for bnd in ['hardlb', 'hardub', 'softlb', 'softub']:
						if bounds_override[index]['override']:
							form.Element(bnd).Update(value=bounds_override[index][bnd])
						else:
							form.Element(bnd).Update(value=bounds[index][bnd])
			bounds_final = copy.deepcopy(bounds)
			# bounds_override['hardlb'] = [min(h, s) for (h, s) in zip(bounds_override['hardlb'], bounds_override['softlb'])]
			# bounds_override['hardub'] = [max(h, s) for (h, s) in zip(bounds_override['hardub'], bounds_override['softub'])]
			for i in range(len(parameter_list)):
				bounds_final[i]['default'] = bounds_override[i]['default']
				bounds_final[i]['tune'] = bounds_override[i]['tune']
				for bnd in ['hardlb', 'hardub', 'softlb', 'softub']:
					if bounds_override[i]['override']:
						bounds_final[i][bnd] = bounds_override[i][bnd]
		else:
			if bounds_final:
				bounds_final = None
				update_plot = True

		if not plt.get_fignums():
			fig_handle = pt.full_plot(dataframe, control=values, bounds=bounds_final, fig=None)
		if update_plot is True:
			update_plot = False
			fig_handle = pt.full_plot(dataframe, control=values, bounds=bounds_final, fig=fig_handle)
		if button == 'Refresh Plot':
			update_plot = True
		elif button == 'Export Bounds JSON':
			if bounds_final is not None:
				with open('bounds.json', 'w') as outfile:
					json.dump(bounds_final, outfile, indent=4)
				sg.Popup('Complete:', 'Bounds.json has been created in working directory.')
			else:
				sg.Popup('Warning:', 'Bounds have not been set')
		elif button == 'Submit Bounds':
			if bounds_final is not None:
				if ms.check_bounds(bounds_final):
					break
			else:
				sg.Popup('Warning:', 'Bounds have not been set')
		elif button == 'Export Best Parameter Sets':
			ms.create_partable(dataframe, values['vlines'])
		elif button == 'Export Selected Parameter Sets':
			ms.create_partable(dataframe, values['parset'])
		elif button == "Import Bounds/Config JSON":
			json_path = get_json_ui()
			if json_path:
				with open(json_path, 'r') as f:
					loaded_json = json.load(f)
				if 'partable' in loaded_json:
					bounds = loaded_json['partable']
				else:
					bounds = loaded_json
				update_plot = True
				custom_bounds = True
	form.Close()
	plt.close(fig_handle)

	return bounds_final


def iteration_plotting_ui(dataframe, visibility=False):
	num_chains = dataframe.MCchain.max() + 1
	chain_list = ['all'] + [x for x in range(num_chains)]

	layout = [
		[sg.Text('MemComputing Inc. Dynamic(Iteration) Plotting Settings')],
		[sg.Text(u"\u203E" * 198, size=(38, 1))],
		[sg.Radio('Instantaneous', 'Radio1', default=True, key='instantaneous'),
		 sg.Radio('Best in History', 'Radio1')],
		[sg.Text('Unsatisfied:'),
		 sg.DropDown(values=['Constraints', 'Distance'], default_value='Constraints', key='_unsat')],
		[sg.Text('Left Y-Axis Scale:'), sg.Combo(values=['linear', 'symlog'], default_value='linear', key='y_scale_l')],
		[sg.Text('Right Y-Axis Scale:'), sg.Combo(values=['linear', 'symlog'], default_value='linear', key='y_scale_r')],

		[sg.Checkbox('Shift Minimum Objective to Zero', key='shift_min')],
		[sg.Text('X-Axis:'),
		 sg.DropDown(values=['Iter', 'TotalWallTime', 'TotalSimTime', 'MCSimTime'], default_value='Iter', key='x_axis')],
		[sg.Text('Chain: '), sg.DropDown(values=chain_list, default_value='all', key='chain')],
		[sg.Button('Refresh Plot'), sg.Text(' ' * 50, size=(20, 1)), sg.Button('Close')]
	]

	form = sg.FlexForm('Standalone Plotter', layout)  # begin with a blank form
	fig_handle = pt.iteration_plot(dataframe, control=None, fig=None)
	while True:

		button, values = form.Read(timeout=500)
		if not plt.get_fignums():
			fig_handle = pt.iteration_plot(dataframe, control=values, fig=None)
		if button == 'Close' or button is None:
			break
		if button == 'Refresh Plot':
			fig_handle = pt.iteration_plot(dataframe, control=values, fig=fig_handle)

	plt.close(fig_handle)

	form.Close()


def standalone_plotting_ui(dataframe_list, visibility=False):
	dataframe_list_copy = dataframe_list.copy()
	if type(dataframe_list_copy[0]) is list:
		num_sets = len(dataframe_list_copy)
		set_names = ['All']
		for num in range(num_sets):
			set_names.append('Set ' + str(num))
		set_button = [sg.Text('Select Parameter Set(s): '), sg.Drop(values=set_names, default_value="Set 0", key='set')]
	else:
		set_button = []
	layout = [
		[sg.Text('MemComputing Inc. Standalone Plotting Settings')],
		[sg.Text(u"\u203E" * 198, size=(38, 1))],
		set_button,
		[sg.Radio('Single Plot', 'Radio1', default=True, key='plot_type'), sg.Radio('Split Plot', 'Radio1')],
		[sg.Radio('Best in History', 'Radio2', default=True, key='best_in_history'),
		 sg.Radio('Instantaneous', 'Radio2')],
		[sg.Radio('Simulation Time', 'Radio3', default=True, key='x_axis'), sg.Radio('CPU Time', 'Radio3')],
		[sg.Text('Y-Axis Scale:'),  sg.Combo(values=['Constraints', 'Distance'], default_value='Constraints', key='which_y_scale'), sg.Combo(values=['linear', 'symlog'], default_value='linear', key='y_scale')],
		[sg.Checkbox('Shift Minimum Objective to Zero', key='shift_min')],
		[sg.Button('Refresh Plot'), sg.Text(' ' * 50, size=(20, 1)), sg.Button('Close')]
	]

	form = sg.FlexForm('Standalone Plotter', layout)  # begin with a blank form
	fig_handle = None
	while True:
		button, values = form.Read(timeout=500)
		if button == 'Close' or button is None:
			break
		if not plt.get_fignums():
			if values['plot_type']:
				fig_handle = pt.standalone_plot(dataframe_list_copy, control=values, fig=None)
			else:
				fig_handle = pt.split_standalone_plot(dataframe_list_copy, control=values, fig=None)

		if button == 'Refresh Plot':
			if values['plot_type']:
				fig_handle = pt.standalone_plot(dataframe_list_copy, control=values, fig=fig_handle)
			else:
				fig_handle = pt.split_standalone_plot(dataframe_list_copy, control=values, fig=fig_handle)

	plt.close(fig_handle)

	form.Close()


def job_history_ui(api):
	job_statuses = {
		9: 'unknown',
		8: 'timeout',
		7: 'error',
		6: 'deleted',
		5: 'cancelled',
		4: 'completed',
		3: 'running',
		2: 'queued',
		1: 'requested'
	}

	if api is None:
		api = core.MemSaaS()
	if not api.has_credentials():
		api.get_credentials_ui()
	if not api.has_credentials():
		return

	response = api.list_my_jobs()
	print(response)
	if response:
		job_result = response.json()
		for num, job in enumerate(job_result):
			if 'status' in job:
				job_result[num]['status'] = job_result[num]['status']['name']

		job_dataframe = pd.DataFrame.from_dict(job_result)
	else:
		job_dataframe = pd.DataFrame(columns=['id', 'name', 'status', 'completed_at'])
	job_dataframe.sort_values(by=['id'], inplace=True, ascending=False)
	# job_dataframe.replace(to_replace={'status': job_statuses}, inplace=True)
	job_id = None
	folder_path = api.default_download_folder
	col_1 = [
		[sg.Table(values=job_dataframe[['id', 'name', 'status', 'completed_at']].values.tolist(), key='job',
		          headings=['ID', 'Name', 'Status', 'Completed'],
		          auto_size_columns=False, col_widths=[4, 16, 8, 12],
		          def_col_width=6, max_col_width=20, justification='left', num_rows=15)]

	]

	col_2 = [
		[sg.Table(values=[], key='files', headings=['File', 'Last Modified', 'Size'],
		          auto_size_columns=False, col_widths=[16, 12, 6], def_col_width=6, max_col_width=20,
		          justification='left', num_rows=13)],
		[sg.Text('Destination Folder:'), sg.In(default_text=folder_path, size=(28, 1), key='folder_path'),
		 sg.FolderBrowse(initial_folder=folder_path)],
		[sg.Text('Auto Unzip Files:'), sg.Checkbox('', default=True, key='unzip'), sg.Text('Remember Download Folder:'),
		 sg.Checkbox('', default=True, key='keep_folder')],
		[sg.Button('Download File'), sg.Button('Download Zip'), sg.Button('Download All')]

	]

	layout = [
		[sg.Text('MemComputing Inc.', font=('default', 14, 'bold'))],
		[sg.Text(vs.api_version)],
		[sg.Column(col_1), sg.VerticalSeparator(pad=[20, 2]), sg.Column(col_2, key='col_2')],
		[sg.Button('Retrieve Job Files'), sg.Button('Refresh'), sg.Button('Cancel Job'), sg.Button('Close')]
	]

	form = sg.FlexForm('Job History', layout)
	while True:

		button, values = form.Read(timeout=500)
		if button == 'Close' or button is None:
			break

		folder_path = values['folder_path']
		if values['keep_folder']:
			api.default_download_folder = folder_path
			api.save_default_folder()
		if button == 'Retrieve Job Files':
			if values['job']:
				job_id = job_dataframe.iloc[values['job'][0]]['id']
				response_file_list = api.list_files(job_id)
				try:
					files_result = response_file_list.json()
				except:
					sg.Popup('Error:', 'Failed to Retrieve Job Files')
					job_id = None
					continue
				file_dataframe = pd.DataFrame.from_dict(files_result)
				form.Element('files').Update(values=file_dataframe.values.tolist())
			else:
				sg.Popup('Warning:', 'No Job Selected')
		if button == 'Cancel Job':
			if values['job']:
				job_id = job_dataframe.iloc[values['job'][0]]['id']
				try:
					r1 = api.cancel_my_job(job_id).json()
					if "message" in r1:
						sg.Popup('Status: ', r1['message'])
					elif 'status' in r1:
						sg.Popup('Status: ', r1['status'])
				except:
					sg.Popup('Status: Failure', "Job cannot be cancelled")
			else:
				sg.Popup('Warning:', 'No Job Selected')
		download_message = ['Download Complete']
		if button == 'Download File':
			if job_id:
				if values['files']:
					file_name = file_dataframe.iloc[values['files'][0]]['name']
					download_message.append(file_name)
					api.get_named_file(job_id, file_name, file_destination=folder_path)
					if values['unzip']:
						ms.unzip_file(file_name, folder_path)
					sg.PopupScrolled(download_message, non_blocking=True)
				else:
					sg.Popup('Warning:', 'No File Selected')
			else:
				sg.Popup('Warning:', 'No Job Selected')
		if button == 'Download Zip':
			if job_id:
				try:
					file_name = file_dataframe[file_dataframe.iloc[:]['name'].str.contains('.zip')][
						'name'].item()
				except ValueError:
					sg.Popup('Error', 'No Zip File Found')
				else:
					download_message.append(file_name)
					api.get_named_file(job_id, file_name, file_destination=folder_path)
					if values['unzip']:
						ms.unzip_file(file_name, folder_path)
					sg.PopupScrolled(download_message, non_blocking=True)
			else:
				sg.Popup('Warning:', 'No Job Selected')
		if button == 'Download All':
			if job_id:
				for file_name in file_dataframe['name']:
					download_message.append(file_name)
					api.get_named_file(job_id, file_name, file_destination=folder_path)
					if values['unzip']:
						ms.unzip_file(file_name, folder_path)
				sg.PopupScrolled(download_message, non_blocking=True)
			else:
				sg.Popup('Warning:', 'No Job Selected')

		if button == 'Refresh':
			response = api.list_my_jobs()
			if response:
				job_result = response.json()
				for num, job in enumerate(job_result):
					if 'status' in job:
						job_result[num]['status'] = job_result[num]['status']['name']
				job_dataframe = pd.DataFrame.from_dict(job_result)
			else:
				job_dataframe = pd.DataFrame(columns=['id', 'name', 'status', 'completed_at'])
			job_dataframe.sort_values(by=['id'], inplace=True, ascending=False)
			# job_dataframe.replace(to_replace={'status': job_statuses}, inplace=True)
			form.Element('job').Update(
				values=job_dataframe[['id', 'name', 'status', 'completed_at']].values.tolist())

	form.Close()


def select_job_ui():
	layout = [
		[sg.Text('MemComputing Inc.', font=('default', 14, 'bold'))],
		[sg.Text(vs.api_version)],
		[sg.Text('Solver Modes', font=('default', 12, 'bold'))]
	]
	# if vs.config_version > 1:
	# 	layout.extend([
	# 	[sg.Text('_' * 50, size=(35, 1))],
	# 	[sg.Button('Parameter Prediction | CPU')],
	# 	[sg.Button('Parameter Prediction | GPU', button_color=('white', 'red'))]
	# 	])
	layout.extend([
		[sg.Text('_' * 50, size=(35, 1))],
		[sg.Button('Standalone | CPU')],
		[sg.Button('Standalone | GPU', button_color=('white', 'red'))],
		[sg.Text('_' * 50, size=(35, 1))],
		[sg.Button('Dynamic Solution Search (DSS) | CPU')],
		[sg.Button('Dynamic Solution Search (DSS) | GPU', button_color=('white', 'red'))],
		[sg.Text('_' * 50, size=(35, 1))],
		[sg.Button('Dynamic Parameter Search (DPS) | CPU')],
		[sg.Button('Dynamic Parameter Search (DPS) | GPU', button_color=('white', 'red'))]
	])

	val = None
	gpu = False
	form = sg.FlexForm('Solver Mode Selection', layout)
	while True:
		button, values = form.Read(timeout=500)
		if button == 'Close' or button is None:
			break
		if button == 'Standalone | CPU':
			val = 0
			gpu = False
			break
		if button == 'Standalone | GPU':
			val = 0
			gpu = True
			break
		if button == 'Dynamic Parameter Search (DPS) | CPU':
			val = 1
			gpu = False
			break
		if button == 'Dynamic Parameter Search (DPS) | GPU':
			val = 1
			gpu = True
			break
		if button == 'Dynamic Solution Search (DSS) | CPU':
			val = 2
			gpu = False
			break
		if button == 'Dynamic Solution Search (DSS) | GPU':
			val = 2
			gpu = True
			break
		if button == 'Parameter Prediction | CPU':
			val = 3
			gpu = False
			break
		if button == 'Parameter Prediction | GPU':
			val = 3
			gpu = True
			break

	form.Close()
	return val, gpu


class SettingsUI:

	def __init__(self, mode: int, gpu: bool, version: int, dataframe=None):
		self.mode = mode
		self.gpu = gpu
		self.dataframe = dataframe
		self.bounds = vs.default_bounds
		self.use_layout = {
			'job_layout': True,
			'main_layout': True,
			'bounds_layout': True if mode else False,
			'mc_layout': True if mode else False,
			# 'parset_layout': True if not mode and dataframe is not None else False,
			'parset_layout': True if not mode else False,
			'advanced_layout': True,
			'nn_layout': True if version > 1 and mode == 3 else False
		}
		#TODO remove cleaned_dataframe uses

		if dataframe is not None:
			self.cleaned_dataframe = ms.sort_values(ms.clean_dataframe(dataframe.copy()), ['S', 'R', 'T'])
		else:
			self.cleaned_dataframe = None
		self.config = SmartConfig(version, mode, gpu + 1)
		self.partable = None

	def job_layout(self):
		job_layout = []
		if self.mode == 0:
			job_layout.append(
				[sg.Text('Standalone Solver', font=('default', 16, 'bold'))]
			)
		elif self.mode == 1:
			job_layout.append(
				[sg.Text('Dynamic Parameter Search', font=('default', 16, 'bold'))]
			)
		elif self.mode == 2:
			job_layout.append(
				[sg.Text('Dynamic Solution Search', font=('default', 16, 'bold'))]
			)
		elif self.mode == 3:
			job_layout.append(
				[sg.Text('Dynamic Parameter Prediction', font=('default', 16, 'bold'))]
			)
		job_layout.extend([
			[sg.Text('Job Settings', font=('default', 12, 'bold'))],
			[sg.Text('Job Name: ', size=(8, 1)),
			 sg.Input(self.config.name, key='name', size=(30, 1))],
			[sg.Text('MPS File: ', size=(8, 1)), sg.In(size=(30, 1), key='mpsfile'),
			 sg.FileBrowse(file_types=[("mps files", "*.mps *.mps.gz")])],
			[sg.Text('Advanced Settings: '), sg.Checkbox('', key='advanced_layout_toggle')],
			[sg.Text('Timeout:', size=(8, 1)),
			 sg.InputText(self.config.timeout if self.config.timeout else "", key="timeout", size=(15, 1)),
			 sg.Text(
				 '(int; range: {} -- {})'.format(default_UI_settings['timeout'][0], default_UI_settings['timeout'][1]),
				 size=(16, 1))],
			[sg.Text('Warmstart File: ', size=(12, 1)), sg.In(size=(25, 1), default_text='', key='warmstart_file'),
			 sg.FileBrowse(file_types=[(".sol files", "*.sol")])]])
		if self.mode:
			job_layout.append(
				[sg.Text('Partable File: ', size=(12, 1)), sg.In(size=(25, 1), default_text='', key='partable_file'),
				 sg.FileBrowse(file_types=[(".json files", "*.json")])])
		if self.mode == 2:
			job_layout.append([sg.Text('Number of Selected Parameter Sets:', size=(27, 1)),
			                  sg.InputText(self.config.nsetsfrompartabledss, key="nsetsfrompartabledss", size=(8, 1)),
			                  sg.Text('(int; range: {} -- {})'.format(default_UI_settings['nsetsfrompartabledss'][0],
			                                                            default_UI_settings['nsetsfrompartabledss'][1]),
			                          size=(16, 1))
			                  ])

		return job_layout

	def main_layout(self):
		main_layout = [
			[sg.Text('General Settings', font=('default', 12, 'bold'))],

			[sg.Text('Sim Time per Iter:', size=(15, 1)),
			 sg.InputText(self.config.simtime, key="simtime", size=(15, 1)),
			 sg.Text('(float; range: {} -- {})'.format(default_UI_settings['simtime'][0],
			                                           default_UI_settings['simtime'][1]),
			         size=(16, 1))],

			[sg.Text('Wall Time per Iter:', size=(15, 1)),
			 sg.InputText(self.config.cputime, key="cputime", size=(15, 1)),
			 sg.Text('(float; range: {} -- {})'.format(default_UI_settings['cputime'][0],
			                                           default_UI_settings['cputime'][1]),
			         size=(16, 1))],

			[sg.Text('Parallel Processes:', size=(15, 1)),
			 sg.Input(self.config.numcores, key="numcores", size=(15, 1)),
			 sg.Text('(int; range: {} -- {})'.format(default_UI_settings['cores'][0], default_UI_settings['cores'][1]),
			         size=(16, 1))]
		]
		return main_layout

	def mc_layout(self):
		mc_layout = [
			[sg.Text('Iterations:', size=(15, 1)),
			 sg.InputText(self.config.iters, key="iters", size=(15, 1)),
			 sg.Text('(int; range: {} -- {})'.format(default_UI_settings['iters'][0], default_UI_settings['iters'][1]),
			         size=(16, 1))],

			[sg.Text('MC Chains:', size=(15, 1)),
			 sg.InputText(self.config.mchains, key="mchains", size=(15, 1)),
			 sg.Text(
				 '(int; range: {} -- {})'.format(default_UI_settings['mchains'][0], default_UI_settings['mchains'][1]),
				 size=(16, 1))
			 ],

			[sg.Text('Min Sigma:', size=(15, 1)),
			 sg.InputText(self.config.sigmamin, key="sigmamin", size=(15, 1)),
			 sg.Text('(float; range: {} -- {})'.format(default_UI_settings['sigmamin'][0],
			                                           default_UI_settings['sigmamin'][1]),
			         size=(16, 1))],

			[sg.Text('Max Sigma:', size=(15, 1)),
			 sg.InputText(self.config.sigmamax, key="sigmamax", size=(15, 1)),
			 sg.Text('(float; range: {} -- {})'.format(default_UI_settings['sigmamax'][0],
			                                           default_UI_settings['sigmamax'][1]),
			         size=(16, 1))]
		]
		if self.mode == 2:
			mc_layout.append([sg.Text('Switch Fraction:', size=(15, 1)),
			                  sg.InputText(self.config.switchfraction, key="switchfraction", size=(15, 1)),
			                  sg.Text('(float; range: {} -- {})'.format(default_UI_settings['switchfraction'][0],
			                                                            default_UI_settings['switchfraction'][1]),
			                          size=(16, 1))
			                  ])
			mc_layout.append([sg.Checkbox('Adaptive Switch Fraction', default=self.config.adaptswitchfraction, key="adaptswitchfraction"),
			                  ])

		return mc_layout

	def nn_layer_dropdown(self):
		num_layers = self.config.numlayers
		layer_dropdown = []
		for layer in range(num_layers):
			layer_dropdown.append(f"Layer {layer}")
		return layer_dropdown

	def nn_display(self):
		num_layers = self.config.numlayers
		nn_display_string = ""
		for layer in range(num_layers):
			nn_display_string += f"Layer {layer:<1} -> Size: {int(self.config.layersize[layer]): <4} | Activation: {self.config.layertype[layer]} \n"
		return nn_display_string

	def nn_layout(self):
		activations = vs.nn_layers
		layer_dropdown = self.nn_layer_dropdown()
		nn_display = self.nn_display()
		nn_layout = [
			[sg.Text('Parameter Prediction Settings', font=('default', 12, 'bold'))],

			[sg.Text('Learning Rate:', size=(12, 1)),
			 sg.InputText(self.config.learningrate, key='learningrate', size=(6, 1)),
			 sg.Text('History Length:', size=(12, 1)),
			 sg.InputText(self.config.histlen, key='histlen', size=(6, 1))],

			[sg.Text('Training Epochs:', size=(12, 1)),
			 sg.InputText(self.config.trainingepoch, key='trainingepoch', size=(6, 1)),
			 sg.Text('Tuning Epochs:', size=(12, 1)),
			 sg.InputText(self.config.tuningepoch, key='tuningepoch', size=(6, 1))],

			[sg.Text('Dropout (par):', size=(12, 1)),
			 sg.InputText(self.config.dropout1, key='dropout1', size=(6, 1)),
			 sg.Text('Dropout (net):', size=(12, 1)),
			 sg.InputText(self.config.dropout2, key='dropout2', size=(6, 1))],

			[sg.Text('Noise:', size=(12, 1)),
			 sg.InputText(self.config.dev, key='dev', size=(6, 1))],

			[sg.Text(u"\u203E" * 198, size=(50, 1))],

			[sg.Text('Network Architecture', font=('default', 12, 'bold'))],

			[sg.Text('Layers:', size=(6, 1)),
			 sg.InputText(self.config.numlayers, key="numlayers", size=(8, 1))],

			[sg.Text('Layer:', size=(6, 1)),
			 sg.DropDown(layer_dropdown, key="layer", default_value=layer_dropdown[0], size=(8, 1)),
			 sg.Text('Size:', size=(4, 1)),
			 sg.InputText(self.config.layersize[0], key='layer_size', size=(6, 1))],

			[sg.Text('Activation Function:', size=(16, 1)),
			 sg.DropDown(activations, key='layer_type', default_value=self.config.layertype[0], size=(10, 1))],

			[sg.Multiline(nn_display, key='nn_display', font='Courier 10', size=(48, 8))]

		]
		return nn_layout

	def bounds_layout(self):
		bounds_layout = [
			[sg.Button('Set Bounds (Plotter)')],
			[sg.Button('Import Bounds (.json)')]
		]
		return bounds_layout

	@staticmethod
	def draw_figure(canvas, figure):
		if canvas.children:
			for child in canvas.winfo_children():
				child.destroy()
		figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
		figure_canvas_agg.draw()
		figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
		return figure_canvas_agg

	def parset_layout(self):
		if self.dataframe is not None:
			dataframe_values = [[index, *items] for index, items in
			                    zip(self.cleaned_dataframe[['S', 'R', 'T']].index.tolist(),
			                        self.cleaned_dataframe[['S', 'R', 'T']].values.tolist())]
		else:
			dataframe_values = [[] for _ in range(4)]

		par_select_ui = [[sg.Button('Import Partable')],
		                 [sg.Text('Select Parameter Set(s): ', size=(25, 1))],
		                 [sg.Table(values=dataframe_values, key='parset',
		                           headings=['Set', 'Distance', 'Objective', 'Stability'],
		                           alternating_row_color='lightblue', col_widths=[3, 8, 8, 8],
		                           auto_size_columns=False, max_col_width=16)],
		                 # [sg.Text('(Caution: If no set of parameters is selected, default values will be used.)',
		                 #          size=(40, 1))],
		                 [sg.Text('Repeats: '), sg.DropDown(['All Sets'], key='repeat_dropdown'),
		                  sg.InputText(vs.global_defaults['repeats'],
		                               key='repeats', size=(4, 1)),
		                  sg.Checkbox('Apply to All', default=True, key='apply_to_all')]]
		# else:
		# 	par_select_ui = []
		return par_select_ui

	def advanced_layout(self):
		advanced_layout = [
			[sg.Text(u"\u203E" * 198, size=(50, 1))],
			[sg.Text('Advanced Settings', font=('default', 12, 'bold'))],

			[sg.Text('Initial Objective:', size=(17, 1)),
			 sg.InputText(self.config.initialobjective, key="initialobjective", size=(15, 1)),
			 sg.Text('(float; range: {} -- {})'.format(default_UI_settings['initialobjective'][0],
			                                           default_UI_settings['initialobjective'][1]), size=(16, 1))],

			[sg.Text('Target Objective:', size=(17, 1)),
			 sg.InputText(self.config.targetobjective, key="targetobjective", size=(15, 1)),
			 sg.Text('(float; range: {} -- {})'.format(default_UI_settings['targetobjective'][0],
			                                           default_UI_settings['targetobjective'][1]), size=(16, 1))],

			[sg.Text('Feasibility Tolerance:', size=(17, 1)),
			 sg.InputText(self.config.feasibilitytolerance, key="feasibilitytolerance", size=(15, 1)),
			 sg.Text('(float; range: {} -- {})'.format(default_UI_settings['feasibilitytolerance'][0],
			                                           default_UI_settings['feasibilitytolerance'][1]), size=(16, 1))],

			[sg.Text('Replicas:', size=(17, 1)),
			 sg.InputText(self.config.replicas, key="replicas", size=(15, 1)),
			 sg.Text('(int; range: {} -- {})'.format(default_UI_settings['replicas'][0],
			                                         default_UI_settings['replicas'][1]),
			         size=(16, 1))],

			[sg.Text('Random Seed:', size=(17, 1)),
			 sg.InputText(self.config.fixedseed, key="fixedseed", size=(15, 1)),
			 sg.Text('(int; range: {} -- {})'.format(default_UI_settings['fixedseed'][0],
			                                         default_UI_settings['fixedseed'][1]),
			         size=(16, 1))],

			[sg.Text('Unary Resolution Base:', size=(17, 1)),
			 sg.InputText(self.config.unaryresbase, key="unaryresbase", size=(15, 1)),
			 sg.Text('(int; range: {} -- {})'.format(default_UI_settings['unaryresbase'][0],
			                                         default_UI_settings['unaryresbase'][1]),
			         size=(16, 1))],

			[sg.Text('Max Expansion Factor:', size=(17, 1)),
			 sg.InputText(self.config.maxexpansionfactor, key="maxexpansionfactor", size=(15, 1)),
			 sg.Text('(float; range: {} -- {})'.format(default_UI_settings['maxexpansionfactor'][0],
			                                         default_UI_settings['maxexpansionfactor'][1]),
			         size=(16, 1))]
		]

		if self.mode == 0:
			advanced_layout.append(
				[sg.Text('Stride:', size=(17, 1)),
				 sg.Input(self.config.stride, key="stride", size=(15, 1)),
				 sg.Text('(int; range: {} -- {})'.format(default_UI_settings['stride'][0],
				                                         default_UI_settings['stride'][1]),
				         size=(16, 1))]
			)

		if self.mode == 2:
			advanced_layout.append([sg.Text('Plateau Limit:', size=(17, 1)),
			                  sg.InputText(self.config.itersdelaythermalx, key="itersdelaythermalx", size=(15, 1)),
			                  sg.Text('(int; range: {} -- {})'.format(default_UI_settings['itersdelaythermalx'][0],
			                                                            default_UI_settings['itersdelaythermalx'][1]),
			                          size=(16, 1))
			                  ])
			advanced_layout.append([sg.Checkbox('Adaptive Objective Function Gap', default=self.config.adaptobjgap, key="adaptobjgap"),
			                  ])

		return advanced_layout

	def __call__(self):
		# Generate Layout
		col_1 = []
		col_2 = []
		if self.use_layout['job_layout']:
			col_1.append([sg.Column(self.job_layout(), key='job_layout')])
			col_1.append([sg.Text(u"\u203E" * 198, size=(50, 1))])
		if self.use_layout['main_layout']:
			col_1.append([sg.Column(self.main_layout(), key='main_layout', pad=(0, 0))])
		if self.use_layout['mc_layout']:
			col_1.append([sg.Column(self.mc_layout(), key='mc_layout', pad=(0, 0))])
		if self.use_layout['bounds_layout']:
			col_2.append([sg.Column(self.bounds_layout(), key='bounds_layout', pad=(0, 0))])
		if self.use_layout['parset_layout']:
			col_2.append([sg.Column(self.parset_layout(), key='parset_layout')])
		if self.use_layout['nn_layout']:
			col_2.append([sg.Column(self.nn_layout(), key='nn_layout')])
		if self.use_layout['advanced_layout']:
			col_2.append([sg.Column(self.advanced_layout(), key='advanced_layout', pad=(0, 0), visible=False)])

		full_layout = [
			[sg.Text('MemComputing Inc.', font=('default', 14, 'bold'))],
			[sg.Text(vs.api_version)],
			[sg.Text(u"\u203E" * 300, size=(110 if col_2 else 52, 1))],
			[sg.Column(col_1), sg.Column(col_2)],

			[sg.Submit(), sg.Button('Export Config'), sg.Button('Close')]]

		form = sg.FlexForm('Solver Settings', full_layout)

		submit = False
		if self.use_layout['parset_layout']:
			repeat_dict = {
				'All Sets': self.config.defaultsize
			}

			control_dict = {
				'last_set': 'All Sets'
			}

		if self.use_layout['nn_layout']:
			nn_list = []
			current_layer = 0
			for layer in range(self.config.numlayers):
				nn_list.append({
					'size': self.config.layersize[layer],
					'type': self.config.layertype[layer]
				})

		missing_entry = None
		while True:
			button, values = form.Read(timeout=500)
			if button == 'Close' or button == 'Exit' or button is None:
				break
			for k, v in values.items():
				if v == '' or v == 'None':
					values[k] = None

			form.Element('advanced_layout').Update(visible=True if values['advanced_layout_toggle'] else False)

			if self.use_layout['parset_layout']:
				# Update repeat dropdown list
				if self.dataframe is not None or self.partable is not None:
					drop_list = []
					if values['apply_to_all']:
						drop_list = ['All Sets']
						set_number = 'All Sets'
					else:
						set_number = re.search(r"Set\s([\d]+)", values['repeat_dropdown'])
						if set_number is None:
							set_number = 'All Sets'
						else:
							set_number = int(set_number.group(1))
						for par in values['parset']:
							drop_list.append(f"Set {par}")

					# Logic for first update after a swap
					if control_dict['last_set'] != set_number:
						control_dict['last_set'] = set_number
						if set_number not in repeat_dict:
							repeat_dict[set_number] = repeat_dict['All Sets']
						form.Element('repeats').Update(repeat_dict[set_number])
					else:
						repeat_dict[set_number] = values['repeats']
					form.Element('repeat_dropdown').Update(values['repeat_dropdown'], values=drop_list)
					form.Element('repeats').Update(repeat_dict[set_number])

			if self.use_layout['nn_layout']:
				if values['numlayers'] is None:
					continue
				self.config.numlayers = int(values['numlayers'])
				nn_layer_dropdown = self.nn_layer_dropdown()
				while len(nn_list) < self.config.numlayers:
					nn_list.insert(0, {
						'size': self.config.layersize[0],
						'type': self.config.layertype[0]
					})
				while len(nn_list) > self.config.numlayers:
					nn_list.pop(0)
				selected_layer = int(re.search(r"Layer\s([\d]+)", values['layer']).group(1))
				if current_layer != selected_layer:
					current_layer = selected_layer
					form.Element('layer_size').Update(nn_list[current_layer]['size'])
					form.Element('layer_type').Update(nn_list[current_layer]['type'])
					continue

				if current_layer >= self.config.numlayers:
					current_layer = 1 - self.config.numlayers
					form.Element('layer').Update(nn_layer_dropdown[current_layer], values=nn_layer_dropdown)
				nn_list[current_layer]['size'] = int(values['layer_size'] if values['layer_size'] else 1)
				nn_list[current_layer]['type'] = values['layer_type']
				self.config.layersize = [layer['size'] for layer in nn_list]
				self.config.layertype = [layer['type'] for layer in nn_list]
				nn_display = self.nn_display()
				form.Element('layer').Update(nn_layer_dropdown[current_layer], values=nn_layer_dropdown)
				form.Element('layer_size').Update(nn_list[current_layer]['size'])
				form.Element('layer_type').Update(nn_list[current_layer]['type'])
				form.Element('nn_display').Update(nn_display)

			# UI Buttons
			if button == 'Set Bounds (Plotter)':
				if self.dataframe is None:
					file_path, version = get_history_ui()
					if file_path is not None:
						self.dataframe = ms.create_dataframe(file_path, version).copy()
						self.cleaned_dataframe = ms.sort_values(ms.clean_dataframe(self.dataframe.copy()),
						                                        ['S', 'R', 'T'])
				if self.dataframe is not None:
					self.bounds = plotting_ui(self.dataframe, visibility=True)
			elif button == 'Import Partable':
				json_path = get_json_ui()
				if json_path:
					with open(json_path, 'r') as f:
						loaded_json = json.load(f)
					if 'numpar' in loaded_json:
						self.partable = loaded_json
					else:
						sg.Popup('Partable is invalid')
				if self.partable is not None:

					form.Element('parset').Update([[[y] if x == 0 else ['----'] for x in range(4)] for y in range(self.partable['numset'])], )
			elif button == 'Import Bounds (.json)':
				self.bounds = ms.get_bounds_json()
			elif button == "Export Config" or button == "Submit":
				# Update Internal Config Settings
				for key, value in values.items():
					if key in self.config:
						try:
							# Change all boolean to int (for now)
							if isinstance(value, bool):
								value = int(value)
							setattr(self.config, key, value)
							form.Element(key).Update(background_color='white')
						except Exception as err:
							print(err)
							if missing_entry is not None and not missing_entry == key:
								form.Element(missing_entry).Update(background_color='white')
							missing_entry = key
							form.Element(key).Update(background_color='red')
							break
					# Warmstart Special Case
					elif key == 'warmstart_file':
						if value:
							setattr(self.config, 'warmstart', 'warmstart.json')

				else:  # No Break
					if self.use_layout['parset_layout']:
						if len(values['parset']) == 0:
							sg.Popup('Warning:', 'No set of parameters is selected, default values will be used instead.')
							# values['parset'] = [0]

						# Clear Partable
						self.config.clear_parsets()

						# Fill in Partable
						for par in values['parset']:
							if self.dataframe is not None:
								par_dict = {
									'repeat': int(
										repeat_dict[par] if par in repeat_dict and not values['apply_to_all'] else
										repeat_dict['All Sets']),
									'parset': self.cleaned_dataframe.loc[par, 'TimePar1': 'StaticPar19'].tolist()
								}
								self.config.add_parset(par_dict)
							elif self.partable is not None:
								par_dict = {
									'repeat': int(
										repeat_dict[par] if par in repeat_dict and not values['apply_to_all'] else
										repeat_dict['All Sets']),
									'parset': self.partable['parset'][str(par)]
								}
								self.config.add_parset(par_dict)

					# Fill Bounds
					self.config.add_bounds(bounds=self.bounds)

					# Fill Defaults if empty
					if not self.config.partable[0]['default']:  # Check if first is empty
						# [x['default'][0] for x in vs.default_bounds]
						self.config.add_parset({'parset': [x['default'][0] for x in vs.default_bounds]})
						# self.config.add_parset({'parset': vs.default_parameters['default']})

					# Validate Config
					self.config.fill()
					valid, err = self.config.validate()
					if valid:
						if button == 'Submit':
							submit = True
							break
						elif button == "Export Config":
							self.config.write()
					else:
						# print(f"The error was: {err}")
						# if missing_entry in values:
						if err['key'] in values:
							if missing_entry is not None and not missing_entry == err['key']:
								form.Element(missing_entry).Update(background_color='white')
							form.Element(err['key']).Update(background_color='red')
							missing_entry = err['key']

		if button is not None:
			form.Close()
		if submit:
			if 'partable_file' in values:
				return self.config, values['warmstart_file'], values['partable_file']
			else:
				return self.config, values['warmstart_file'], None
		else:
			return None, None, None


if __name__ == "__main__":

	file_path, version = get_history_ui()
	if file_path is not None:
		dataframe = ms.create_dataframe(file_path, version)
	else:
		dataframe = None
	val, gpu = select_job_ui()
	val = 3
	window = SettingsUI(val, gpu, 2, dataframe=dataframe)
	test = window()
	if test is not None:
		test.write()
