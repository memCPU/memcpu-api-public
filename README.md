# **MemCPU API - Public**

### Welcome to the public repository of the MemCPU(tm) sample Python API source code.  This sample source code provides an example of how to use and interact with the MemCPU XPC SaaS API.  The intended use of our API is to allow our customers to integrate and automate use of MemComputing XPC SaaS into their own applications, tools and processes.  Using the API, you will be able to programmatically submit and monitor jobs, upload job and configuration files, and download results files. Please refer to the memAPI folder and specifically the core.py Python source file for the objects which demonstrate using the MemCPU API.  Documentation on the full MemCPU API may also be found on our Swagger Hub site here: https://api.memcpu.io/swagger/

### The MemCPU API sample program is also a fully funtional and interactive appliation which allows you to interactively submit your jobs, monitor their progress, analyze and visualize the results files in order to find optimal tuning parameters for your optimization problems.  The plotting routines are also freely available for you to use and embed into your own applications.  Please refer to our MemCPU API tutorial video for additional details here:  https://www.youtube.com/playlist?list=PLh6oeuEjCddCFnVpBmCosWRqOV_E0He05

### This public repository is expected to be updated frequently, so please check back often for updated versions.

## This sample requires Python version 3.6 or 3.7.  Required python packages (dependencies) are specified in the supplied requirements.txt file.

To install, run the following command within the working directory or within your venv to install the necessary
packages.

```
pip install -r requirements.txt
```

(Update: Oct 31, 2019) There is a current incompatibility between certain packages and python 3.8. Suggested Python version <= 3.7

# License
Please see our open source LICENSE file, in this repository:
    https://bitbucket.org/memCPU/memcpu-api-public/src/c1278fd4f336df581c5b7bcea2d540f2707decf4/LICENSE

# Terms of Use
Access to and use of this public MemCPU API source code repository is granted to our existing customers for their explicit use to integrate and automate the MemComputing XPC SaaS with their own applications, tools, systems and processes.  Your access to, download of, and use of this sample source code is subject to the same Terms and Conditions to which you previously agreed when you initially registered for an account on the MemComputing XPC SaaS Platform.  MemComputing, Inc Terms and Conditions are located here for your reference:  https://app.memcpu.io/terms_and_conditions

An excerpt of the MemComputing, Inc. Terms of Conditions with respect to Allocation of Risk is as follows:

## Scheduled Maintenance 
Each Party represents to the other (i) that the execution and performance of its obligations under this Agreement will not conflict with or violate any Applicable Laws or any agreement or order by which the representing Party is bound; and (ii) that this Agreement, when executed and delivered, will constitute a valid and binding obligation of such Party and will be enforceable against such Party in accordance with its terms.

## Disclaimers 
(a) CUSTOMER REPRESENTS THAT IT IS ENTERING THIS AGREEMENT WITHOUT RELYING UPON ANY PROVIDER REPRESENTATION OR WARRANTY NOT EXPRESSLY STATED IN THIS AGREEMENT. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, PROVIDER DISCLAIMS ANY AND ALL PROMISES, REPRESENTATIONS AND WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, DATA ACCURACY, SYSTEM INTEGRATION, SYSTEM RELIABILITY, TITLE, NON-INFRINGEMENT, NON-INTERFERENCE AND/OR QUIET ENJOYMENT, AND ALL WARRANTIES THAT MAY OTHERWISE BE IMPLIED. NO WARRANTIES ARE MADE ON THE BASIS OF TRADE USAGE, COURSE OF DEALING, OR COURSE OF PERFORMANCE.

(b) CUSTOMER ASSUMES COMPLETE RESPONSIBILITY, WITHOUT ANY RECOURSE AGAINST PROVIDER, FOR THE SELECTION OF THE SAAS TO ACHIEVE CUSTOMER’S INTENDED RESULTS AND FOR ITS USE IN CUSTOMER’S BUSINESS OF THE PROVIDER CONTENT, INCLUDING WITHOUT LIMITATION ALL RESULTS OBTAINED FROM THE USE OF THE SAAS. CUSTOMER ACKNOWLEDGES THAT IT IS SOLELY RESPONSIBLE FOR THE PROVIDER CONTENT, INCLUDING WITHOUT LIMITATION ALL RESULTS OBTAINED FROM THE USE OF THE SAAS, INCLUDING THE COMPLETENESS, ACCURACY, AND CONTENT OF SUCH RESULTS. PROVIDER DOES NOT WARRANT THAT THE SAAS WILL MEET CUSTOMER’S REQUIREMENTS, THAT THE OPERATION OF THE SAAS WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT ALL ERRORS WILL BE CORRECTED.

## Indemnification of Customer by Provider 
Provider agrees to defend, indemnify, and hold harmless Customer from and against all third-party claims and actions (collectively, “Claims” and individually, a “Claim”) that may, at any time, arise out of or relate to a breach by Provider of any of its representations given above.

## Indemnification of Provider by Customer 
Except to the extent of any Claims in respect of which Provider is obligated to indemnify Customer under above section, Customer agrees to defend, indemnify and hold harmless Provider and its affiliates from and against all Claims, that may, at any time, arise out of or relate to use of the SaaS or any Provider Content by or on behalf of Customer.

Except as expressly provided in this Section, neither Party shall have any liability under or in connection with this Agreement for any indirect, incidental, consequential, special, exemplary, or punitive damages, nor any liability for lost profits, loss of data, loss of business opportunity, or business interruption, regardless of the theory of liability (including theories of contractual liability, tort liability, or strict liability), even if the liable Party knew or should have known that those kinds of damages were possible. IN ADDITION, provider’s maximum cumulative liability under or in connection with this Agreement shall never exceed AN injured Party’s actual direct damages, capped at an amount equal to $1000. The foregoing limitations of liability shall not be applicable to a Party’s indemnification obligations under this Section 4 or to any damages that the liable Party is not permitted to disclaim (or, as applicable, limit) under Applicable Law. Customer acknowledges that this Section is an essential part of this Agreement, absent which the economic terms and other provisions of this Agreement would be substantially different.

