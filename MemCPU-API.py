import memAPI.versions
from memAPI import misc
from UI import *
from memAPI import core

###########################################

main_form = sg.FlexForm('MemComputing Inc.')

layout = [
    [sg.Text('MemComputing Inc.', font=('default', 14, 'bold'))],
    [sg.Text(memAPI.versions.api_version)],
    [sg.Button('Login Credentials')],
    [sg.Text('_' * 50, size=(35, 1))],
    [sg.Text('MemCPU API', font=('default', 12, 'bold'))],
    [sg.Button('Create New Job'), sg.Checkbox('Use Previous History File', key='get history')],
    [sg.Button('View All Jobs')],
    [sg.Text('_' * 50, size=(35, 1))],
    [sg.Text('Plotters', font=('default', 12, 'bold'))],
    [sg.Button('Dynamic (Parameters)'), sg.Button('Dynamic (Iterations)')],
    [sg.Button('Standalone')]
    # [sg.Text('_' * 50, size=(35, 1))],
    # [sg.Button('Extract Partable')]
]

main_form.Layout(layout)
SaaS = core.MemSaaS()
while True:
    dataframe = None
    dataframe_list = []
    bounds = None
    button, settings = main_form.Read()
    if button is None or button == 'Close':
        break
    if button == 'Login Credentials':
        SaaS.get_credentials_ui()
    if button == 'Dynamic (Parameters)':
        if dataframe is None:
            out = get_history_ui()
            if out[0] is not None:
                dataframe = ms.create_dataframe(*out)
        if dataframe is not None:
            bounds = plotting_ui(dataframe)
    if button == 'Dynamic (Iterations)':
        if dataframe is None:
            out = get_history_ui()
            if out[0] is not None:
                dataframe = ms.create_dataframe(*out)
        if dataframe is not None:
            iteration_plotting_ui(dataframe)
    elif button == 'Standalone':
        file_paths, versions = get_history_ui(standalone=True)
        dataframe_list = misc.standalone_dataframe_list(file_paths, versions)
        if dataframe_list:
            standalone_plotting_ui(dataframe_list)
    elif button == 'View All Jobs':
        job_history_ui(api=SaaS)
    # elif button == 'Extract Partable':
    #     ExtractPartableUI(api=SaaS)
    elif button == 'Create New Job':
        while True:
            values = None
            mode, gpu = select_job_ui()
            if mode is None:
                break
            if settings['get history']:
                file_path, version = get_history_ui()
                if file_path is not None:
                    dataframe = ms.create_dataframe(file_path, version)
            window = SettingsUI(mode, gpu, memAPI.versions.config_version, dataframe=dataframe)
            config, warmstart, partable = window()
            if config:
                if not SaaS.has_credentials():
                    SaaS.get_credentials_ui()
                if SaaS.has_credentials():
                    config_file_path = config.write()
                    SaaS.create_job(config.mpsfile, os.path.join(os.getcwd(), config_file_path), warmstart, partable)

                    break
            else:
                break

main_form.Close()
###########################################
