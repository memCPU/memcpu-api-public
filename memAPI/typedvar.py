class Var:
	def __init__(self, name=None):
		self.name = name

	def __set__(self, instance, value):
		instance.__dict__[self.name] = value

	def __get__(self, instance, owner):
		return instance.__dict__[self.name]


class Typed(Var):
	ty = type

	def __set__(self, instance, value):
		if not isinstance(value, self.ty):
			if value is not None:
				try:
					value = self.ty(value)
				except TypeError:
					raise TypeError(f"Expected {self.ty}, got {type(value)} for {self.name} instead.")
		super().__set__(instance, value)


class Functioned(Var):
	def __init__(self, *args, func, **kwargs):
		super().__init__(*args, **kwargs)
		self.function = func

	def __set__(self, instance, value):
		if not self.function(instance, value):
			raise ValueError(f"{value} is not a valid value for {self.name}")
		super().__set__(instance, value)


class Bounded(Var):
	def __init__(self, *args, bounds: list, **kwargs):
		super().__init__(*args, **kwargs)
		self.bounds = bounds

	def __set__(self, instance, value):
		if self.bounds[0]:
			if not self.bounds[0] <= value:
				raise ValueError(f"Value for {self.name} must be within range {self.bounds}")
		if self.bounds[1]:
			if not self.bounds[1] >= value:
				raise ValueError(f"Value for {self.name} must be within range {self.bounds}")
		super().__set__(instance, value)


class Bool(Typed):
	ty = bool


class List(Typed):
	ty = list


class Int(Typed):
	ty = int


class Float(Typed):
	ty = float


class Str(Typed):
	ty = str


class FloatBounded(Float, Bounded):
	pass


class IntBounded(Int, Bounded):
	pass


class IntBoundedFunctioned(IntBounded, Functioned):
	pass


class StrFunctioned(Str, Functioned):
	pass


class ListFunctioned(List, Functioned):
	pass


class Test:
	x = IntBounded('x', bounds=[1, 10])
	y = Int('y')
	z = IntBoundedFunctioned('z', bounds=[1, 100], func=lambda x, y: True if x.x == 1 else False)
	t = type('IBF', (Int, Bounded, Functioned), {})('t', bounds=[1, 100], func=lambda x, y: True)

	def __init__(self):
		self.x = 1
		self.y = 5
		self.z = 4
		self.t = 1


if __name__ == "__main__":
	test = Test()
