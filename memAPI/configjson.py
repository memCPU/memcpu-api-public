import json
import os
import memAPI.versions as vs
import memAPI.misc as ms


class SmartConfig:
	def __init__(self, version: int, mode: int, instance: int) -> None:
		self.validator = vs.Validator(version, mode, instance)
		# Create Type Checking Attributes
		for key, value in ms.traverse_nested_dict(self.validator.get_types()):
			if value:
				setattr(SmartConfig, key, value)

		for key, value in ms.traverse_nested_dict(self.validator.get_config()):
			setattr(self, key, value)

	def __contains__(self, item):
		if item in self.__dict__:
			return True
		else:
			return False

	def __getitem__(self, item):
		return getattr(self, item)

	def __setitem__(self, key, value):
		setattr(self, key, value)

	def __getattribute__(self, key):
		return super().__getattribute__(key)
		# Smart Name Append
		# if key in ["assignfile", "plotfile", "stdout"]:
		# 	return "".join(filter(None, (self.name, super().__getattribute__(key))))
		# else:
		# 	return super().__getattribute__(key)

	def items(self):
		for key in self.__dict__.keys():
			yield key, self[key]

	def validate(self):
		validator = self.validator.get_validation()
		for key, value in self.items():
			err_key = key
			try:
				# Check Required
				if key not in validator['not_required']:
					assert value is not None, \
						f"{key} is required, but 'None' was provided"
				elif value is None:
					continue

				if key in validator['validate']:
					assert validator['validate'][key](self), \
						f"Key {key} failed validator: {self[key]}"
			except (AssertionError, AttributeError, TypeError, ValueError) as err:
				print('Validation Failure!')
				print(f"Error is : {err}")
				return False, {'key': err_key, 'err': err}
		else:
			print('Validation Success!')
			return True, None

	def fill(self):
		for k, v in self.validator.get_smart_defaults().items():
			if self[k] is None:
				self[k] = v(self)

	def clear_parsets(self):
		self.defaultsize = []
		for par in range(self.partablesize):
			self.partable[par]['default'] = []

	def add_parset(self, args):
		"""
		Takes in a dictionary for each parset. Can take a list of dictionaries as well.
		dict = {
			'repeat': int, (optional, will fill from defaults if not present)
			'parset': [parameter set]
		}
		"""
		if isinstance(args, list):
			for arg in args:
				self.add_parset(arg)
		elif isinstance(args, dict):
			if 'repeat' in args:
				repeat = args['repeat']
			else:
				repeat = vs.global_defaults['repeats']
			parset = args['parset']
			assert type(repeat) is int, f"Expected {type(int)} for value of repeat, got {type(repeat)} instead."
			assert type(parset) is list, f"Expected {type(list)} for value parset, got {type(parset)} instead."
			assert len(parset) == self.partablesize, f"Expected parset of length {self.partablesize}, " \
				f"got length {len(parset)} instead."
			self.defaultsize.append(repeat)
			for par in range(self.partablesize):
				self.partable[par]['default'].append(parset[par])

	def add_bounds(self, bounds):
		for i in range(self["partablesize"]):
			self.partable[i]["hardlb"] = bounds[i]['hardlb']
			self.partable[i]["softlb"] = bounds[i]['softlb']
			self.partable[i]["softub"] = bounds[i]['softub']
			self.partable[i]["hardub"] = bounds[i]['hardub']
			self.partable[i]["tune"] = bounds[i]['tune']

	def write(self, directory=None):
		config = self.validator.get_config()
		ms.update_nested_dict(config, dict(self.items()))
		if directory:
			file = os.path.join(directory, "config.json")
		else:
			file = "config.json"
		with open(file, 'w') as outfile:
			json.dump(config, outfile, indent=4)
		return file


if __name__ == "__main__":
	test = SmartConfig(1, 0, 1)

