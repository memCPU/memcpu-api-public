import re
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import numpy as np
import math
import matplotlib
from memAPI.misc import sort_values, get_active, get_parameters
import memAPI.versions as vs
matplotlib.use('TkAgg')
plt.ioff()


def get_color_map(number_of_colors, color_map='tab10'):
    values = range(number_of_colors)
    c_norm = colors.Normalize(vmin=0, vmax=values[-1])
    c_map = plt.get_cmap(color_map)
    scalar_map = cmx.ScalarMappable(norm=c_norm, cmap=c_map)
    return scalar_map, values


def data2bin(dataframe, bins=None, key='LowestUnsatConstr'):
    parameter_list = get_parameters(dataframe)
    if bins is None:
        bins = 20
    bin_list = {}
    empty_list = {}
    out = [np.full(shape=(bins, 2), fill_value=np.Inf) for _ in range(len(parameter_list))]
    for x in range(len(parameter_list)):
        mn, mx = dataframe[parameter_list[x]].min(), dataframe[parameter_list[x]].max()
        # Below picks upper and lower based on nearest whole number
        # mn, mx = math.floor(dataframe[parameter_list[x]].min()), math.ceil(dataframe[parameter_list[x]].max())

        sorted_data = sort_values(dataframe, parameter_list[x])

        # add percent to ends smaller than bins (fix floating point bug)
        diff = 0.5 * (mx - mn) / bins
        mn = mn - diff
        mx = mx + diff

        bin_list[x] = [mn + x * (mx - mn) / bins for x in range(bins + 1)]
        empty_list[x] = []
        current_bin = 1
        for (param_value, s, r) in zip(sorted_data[parameter_list[x]], sorted_data[key],
                                       sorted_data['LowestObj']):
            while not param_value <= bin_list[x][current_bin]:
                current_bin += 1

            out[x][current_bin - 1] = np.minimum(out[x][current_bin - 1], (s, r))
    for b in range(len(parameter_list)):
        out[b] = np.where(out[b] == np.inf, np.nan, out[b])

    return out, bin_list


def standalone_plot(dataframe_list, control=None, fig=None):
    dataframe_list_copy = dataframe_list
    fig = clean_figure(1, fig=fig)

    if control is None:
        control = {
            'x_axis': True,
            'best_in_history': True,
            'set': 'All',
            'y_scale': 'linear',
            'which_y_scale': 'Constraints'
        }
    if 'set' not in control:
        control['set'] = 'All'

    if control['x_axis']:
        control['x_axis'] = 'SimTime'
        name_str = "Simulation Time"

    else:
        control['x_axis'] = 'WallTime'
        name_str = "CPU Time"

    if control['which_y_scale'] == 'Constraints':
        y_axis_regex = '\SUnsatConstr'
    elif control['which_y_scale'] == 'Distance':
        y_axis_regex = '\SUnsatDist'
    else:
        y_axis_regex = '\SUnsatConstr'

    if control['set'] == 'All':
        # Flatten Dataframe List
        if type(dataframe_list_copy[0]) is list:
            dataframe_list_copy = [val for sublist in dataframe_list_copy for val in sublist]
    else:
        which_set = int(re.search(r'Set\s([\d]*)', control['set']).group(1))
        dataframe_list_copy = dataframe_list_copy[which_set]

    max_unsat = -np.inf

    # Constraints (Blue, left axis) #
    left_axis_columns = dataframe_list_copy[0].filter(regex=y_axis_regex).columns
    fig.axes[0].cla()
    fig.axes[0].twinx()
    for dataframe in dataframe_list_copy:
        max_unsat = max(np.max(dataframe[left_axis_columns].cummin().values), max_unsat)
        if control['best_in_history']:
            fig.axes[0].plot(dataframe[control['x_axis']], dataframe[left_axis_columns].cummin(), 'b-')
        else:
            fig.axes[0].plot(dataframe[control['x_axis']], dataframe[left_axis_columns], 'bo', markersize=2)

    # Objective (Orange, right axis) #
    right_axis_columns = dataframe_list_copy[0].filter(regex='\SObj').columns

    if control['shift_min']:
        objective_shift = np.inf
        for dataframe in dataframe_list_copy:
            objective_shift = min(dataframe[right_axis_columns].min().min(), objective_shift)
    else:
        objective_shift = 0

    fig.axes[1].cla()
    for dataframe in dataframe_list_copy:
        if control['best_in_history']:
            fig.axes[1].plot(dataframe[control['x_axis']], dataframe[right_axis_columns].cummin() - objective_shift, 'C1-')
        else:
            fig.axes[1].plot(dataframe[control['x_axis']], dataframe[right_axis_columns] - objective_shift, 'C1o', markersize=2)

    fig.axes[0].set_title('Objective/Constraint (Best) vs ' + name_str + ' | Set : ' + str(control['set']), y=1.04)
    fig.axes[0].set_xlabel(control['x_axis'])
    fig.axes[0].set_ylabel(control['which_y_scale'])
    fig.axes[1].set_ylabel('Objective')
    fig.axes[0].yaxis.label.set_color('b')
    fig.axes[0].tick_params(axis='y', colors='b')
    fig.axes[1].yaxis.label.set_color('C1')
    fig.axes[1].tick_params(axis='y', colors='C1')
    fig.axes[0].ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    fig.axes[1].ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    if control['y_scale'] == 'symlog':
        fig.axes[0].set_yscale('symlog', subs=[1, 2, 3, 4, 5, 6, 7, 8, 9])
        fig.axes[1].set_yscale('symlog', subs=[1, 2, 3, 4, 5, 6, 7, 8, 9])
    else:
        fig.axes[0].set_yscale('linear')
        fig.axes[1].set_yscale('linear')
    # fig.axes[0].set_ylim(auto=True)
    try:
        fig.axes[0].set_ylim([0, max_unsat])
    except ValueError:
        pass
    plt.show(block=False)

    return fig


def clean_figure(n_data, n_col=2, fig=None):
    if n_data < n_col:
        columns = n_data
    else:
        columns = n_col
    rows = int(np.ceil(n_data/columns))
    del_row = int(np.floor(n_data/columns))
    if fig is None:
        fig, axs = plt.subplots(rows, columns, tight_layout=True)
        if del_row == 0:
            for x in range(rows*columns - n_data):
                fig.delaxes(axs[(columns - 1) - x])
        else:
            for x in range(rows*columns - n_data):
                fig.delaxes(axs[del_row, (columns - 1) - x])
    else:
        # Clear old axes #
        for num, ax in enumerate(fig.axes):
            ax.cla()
            ax.remove()
        axs = fig.subplots(rows, columns, squeeze=True)
        if del_row == 0:
            for x in range(rows*columns - n_data):
                fig.delaxes(axs[(columns - 1) - x])
        else:
            for x in range(rows*columns - n_data):
                fig.delaxes(axs[del_row, (columns - 1) - x])

    return fig


def split_standalone_plot(dataframe_list, control=None, fig=None):
    dataframe_list_copy = dataframe_list

    if control is None:
        control = {
            'x_axis': True,
            'best_in_history': True,
            'set': 'All',
            'shift_min': False
        }
    if 'set' not in control:
        control['set'] = 'All'

    if control['x_axis']:
        control['x_axis'] = 'SimTime'

    else:
        control['x_axis'] = 'WallTime'

    if control['which_y_scale'] == 'Constraints':
        y_axis_regex = '\SUnsatConstr'
    elif control['which_y_scale'] == 'Distance':
        y_axis_regex = '\SUnsatDist'
    else:
        y_axis_regex = '\SUnsatConstr'

    if not control['set'] == 'All':
        which_set = int(re.search(r'Set\s([\d]*)', control['set']).group(1))
        dataframe_list_copy = dataframe_list_copy[which_set]
        # # Flatten Dataframe List
        # dataframe_list_copy = [val for sublist in dataframe_list_copy for val in sublist]

    data_length = len(dataframe_list_copy)
    fig = clean_figure(data_length, fig=fig)

    if type(dataframe_list_copy) is list:
        if type(dataframe_list_copy[0]) is list:
            left_axis_columns = next(filter(None, dataframe_list_copy))[0].filter(regex=y_axis_regex).columns
            right_axis_columns = next(filter(None, dataframe_list_copy))[0].filter(regex='\SObj').columns
        else:
            left_axis_columns = dataframe_list_copy[0].filter(regex=y_axis_regex).columns
            right_axis_columns = dataframe_list_copy[0].filter(regex='\SObj').columns
    else:
        left_axis_columns = dataframe_list_copy.filter(regex=y_axis_regex).columns
        right_axis_columns = dataframe_list_copy.filter(regex='\SObj').columns

    if control['shift_min']:
        objective_shift = np.inf
        for dataframe in dataframe_list_copy:
            objective_shift = min(dataframe[right_axis_columns].min().min(), objective_shift)
    else:
        objective_shift = 0


    # Constraints (Blue, left axis) #
    for num, (ax, dataframe) in enumerate(zip(fig.axes, dataframe_list_copy)):
        ax2 = ax.twinx()
        ax.cla()
        ax2.cla()

        if type(dataframe) is not list:
            dataframe = [dataframe]

        max_unsat = -np.inf
        for df in dataframe:
            if control['best_in_history']:
                ax.plot(df[control['x_axis']], df[left_axis_columns].cummin(), 'b-')
            else:
                ax.plot(df[control['x_axis']], df[left_axis_columns], 'bo', markersize=2)
            max_unsat = max(np.max(df[left_axis_columns].cummin().values), max_unsat)
            # Objective (Orange, right axis) #
            if control['best_in_history']:
                ax2.plot(df[control['x_axis']], df[right_axis_columns].cummin() - objective_shift, 'C1-')
            else:
                ax2.plot(df[control['x_axis']], df[right_axis_columns] - objective_shift, 'C1o', markersize=2)

        if control['set'] == "All":
            ax.set_title("Set {0}".format(num), y=1.04)
        else:
            ax.set_title("{0} - Core {1}".format(control['set'], num), y=1.04)
        ax.set_xlabel(control['x_axis'])
        ax.set_ylabel(control['which_y_scale'])
        ax2.set_ylabel('Objective')
        ax.yaxis.label.set_color('b')
        ax.tick_params(axis='y', colors='b')
        ax2.yaxis.label.set_color('C1')
        ax2.tick_params(axis='y', colors='C1')
        ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        ax2.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        if control['y_scale'] == 'symlog':
            ax.set_yscale('symlog', subs=[1, 2, 3, 4, 5, 6, 7, 8, 9])
            ax2.set_yscale('symlog', subs=[1, 2, 3, 4, 5, 6, 7, 8, 9])
        else:
            ax.set_yscale('linear')
            ax2.set_yscale('linear')
        try:
            ax.set_ylim([0, max_unsat])
        except ValueError:
            pass
        ax2.set_xlim(auto=True)
    plt.show(block=False)

    return fig


def full_plot(dataframe, control=None, bounds=None, fig=None):
    if control:
        vlines = control['vlines']
        data_points = control['data_points']
        bins = control['bins']
    else:
        control = {}
        control['histogram_plot'] = False
        control['sr_plot'] = False
        control['vlines_plot'] = False
        control['unsat_dist'] = False
        control['y_scale'] = 'linear'
        control['which_y_scale'] = 'Objective'
        control['y_offset'] = 0
        control['use_y_min'] = False
        control['use_y_max'] = False
        control['y_min'] = None
        control['y_max'] = None
        control['parset_plot'] = False
        vlines = 10
        data_points = 200
        bins = 25

    if control['parset_plot']:
        if 'parset' not in control:
            control['parset'] = []

    parameter_list = get_parameters(dataframe)
    active_params = get_active(dataframe)

    data_sorted = sort_values(dataframe, ['LowestUnsatDist', 'LowestObj', 'MCSimTime'])
    if control['unsat_dist']:
        unsat_key = 'LowestUnsatDist'
    #     data_sorted = sort_values(dataframe, ['LowestUnsatDist', 'LowestObj', 'MCSimTime'])
    else:
        unsat_key = 'LowestUnsatConstr'
    #     data_sorted = sort_values(dataframe, ['LowestUnsatConstr', 'LowestObj', 'MCSimTime'])



    # Create Fig Handle #
    if fig is None:
        fig, axs = plt.subplots(4, 5, tight_layout=True)
        for x in range(20 - int(len(parameter_list))):
            fig.delaxes(axs[3, 4 - x])

    # Clear old axes #
    for num, ax in enumerate(fig.axes):
        ax.cla()
        if num > (len(parameter_list) - 1):
            ax.remove()
        else:
            ax.set_title(parameter_list[num])
            ax.set_ylim([-0.05, 1])

    #Fig Title
    fig.suptitle(dataframe.file_name)

    # Plot vlines #
    if control['vlines_plot']:
        scalar_map, values = get_color_map(vlines if vlines != 0 else 10)
        for num, (ax, parameter, active) in enumerate(zip(fig.axes, parameter_list, active_params)):
            for line in range(vlines):
                ax.axvline(data_sorted[parameter][line], linewidth=1.5, color=scalar_map.to_rgba(values[line]))

    # Plot selected vlines #
    if control['parset_plot']:
        scalar_map, values = get_color_map(len(control['parset']) if len(control['parset']) != 0 else 10)
        for num, (ax, parameter, active) in enumerate(zip(fig.axes, parameter_list, active_params)):
            for num2, line in enumerate(control['parset']):
                print(line)
                print(f"par {num2} | {data_sorted[parameter][line]}")
                ax.axvline(data_sorted[parameter][line], linewidth=1.5, color=scalar_map.to_rgba(values[num2]))

    # SR Plot #
    if control['sr_plot']:
        # SR - Plot #
        oc_data, bin_edges = data2bin(dataframe, bins, key=unsat_key)
        bin_list = {k: [(v[x] + v[x + 1]) / 2 for x in range(len(v) - 1)] for (k, v) in bin_edges.items()}

        # No Data Regions
        no_data = {}
        for num, par in enumerate(parameter_list):
            no_data[num] = []
            start = None
            end = None
            for num2, data in enumerate(oc_data[num]):
                if all(np.isnan(data)):
                    if start is None:
                        start = bin_edges[num][num2]
                    end = bin_edges[num][num2 + 1]
                else:
                    if start is not None:
                        no_data[num].append((start, end))
                        start = None
                        end = None
            if start is not None:
                no_data[num].append((start, end))

        for num, (ax, parameter, active) in enumerate(zip(fig.axes, parameter_list, active_params)):
            ax2 = ax.twinx()

            if active:
                ax.plot(bin_list[num], oc_data[num][:, 0], 'bo-', markersize=2)
                for region in no_data[num]:
                    ax.axvspan(*region, fill=None, edgecolor=None, alpha=0.1, hatch='\\')
            else:
                ax.text(.5, .5, 'Parameter Not Tuned \n ({:.5f})'.format(data_sorted[parameter][0]),
                        horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
            if all(np.isnan(oc_data[num][:, 0])):
                ymax = 1
            else:
                ymax = np.max(oc_data[num][:, 0][~np.isnan(oc_data[num][:, 0])]) + 1
            ymin = -ymax / 20

            if active:

                ax.set_xlim([np.min(bin_edges[num]), np.max(bin_edges[num])])
            # ax.set_xlim(auto=True)

            if control['unsat_dist']:
                ax.set_ylabel('Distance from Solution')
            else:
                ax.set_ylabel('Unsatisfied Constraints')
            ax.set_title(parameter_list[num])
            ax.yaxis.label.set_color('b')
            ax.tick_params(axis='y', colors='b')
            ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
            if control['which_y_scale'] == 'Constraints':
                if control['y_scale'] == 'symlog':
                    ax.set_yscale('symlog', subs=[1, 2, 3, 4, 5, 6, 7, 8, 9])
                    ymin = -1
                else:
                    ax.set_yscale('linear')
            try:
                ax.set_ylim([ymin, ymax])
            except ValueError:
                pass

            # Y Offset
            oc_data[num][:, 1] = list(map(lambda x: x + control['y_offset'], oc_data[num][:, 1]))

            if active:
                ax2.plot(bin_list[num], oc_data[num][:, 1], 'C1o-', markersize=2)

            if all(np.isnan(oc_data[num][:, 1])):
                ymax = 1
                ymin = -ymax / 20
            else:
                ymax = np.max(oc_data[num][:, 1][~np.isnan(oc_data[num][:, 1])]) + 1
                ymin = np.min(oc_data[num][:, 1][~np.isnan(oc_data[num][:, 1])]) - 1

            if control['use_y_min']:
                ymin = control['y_min']
            if control['use_y_max']:
                ymax = control['y_max']

            ax2.set_ylabel('Objective')
            ax2.set_title(parameter_list[num])
            ax2.yaxis.label.set_color('C1')
            ax2.tick_params(axis='y', colors='C1')
            ax2.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))

            if control['which_y_scale'] == 'Objective':
                if control['y_scale'] == 'symlog':
                    ax2.set_yscale('symlog', subs=[1, 2, 3, 4, 5, 6, 7, 8, 9])
                    # Test if data spans an order of magnitude
                    if ymin > 0 and ymax > 0:
                        min_log = math.ceil(math.log10(ymin))
                        max_log = math.floor(math.log10(ymax))
                        if min_log >= max_log:
                            ymax = 10 ** (max_log + 1)
                            ymin = 10 ** (min_log - 1)
                else:
                    ax2.set_yscale('linear')
            try:
                ax2.set_ylim([ymin, ymax])
            except ValueError:
                pass


    # Histogram Plot #
    if control['histogram_plot']:
        # Histogram #
        hist_data_points = data_points

        for num, (ax, parameter, active) in enumerate(zip(fig.axes, parameter_list, active_params)):
            if active:
                ylims = ax.get_ylim()
                xlims = ax.get_xlim()
                counts, hist_bins = np.histogram(data_sorted[parameter][:hist_data_points], bins=bins, range=xlims)
                counts = (counts / np.max(counts)) * (ylims[1] - ylims[0]) * .7
                # counts = (counts / np.max(counts)) * .7
                ax.hist(hist_bins[:-1], hist_bins, weights=counts, bottom=ylims[0], color='blue', alpha=0.3)
            else:
                ax.text(.5, .5, 'Parameter Not Tuned \n ({:.5f})'.format(data_sorted[parameter][0]),
                        horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
            ax.set_title(parameter_list[num])

    # Plot Bounds #
    if bounds is not None and control['bounds_plot']:
        for num, (ax, parameter, active) in enumerate(zip(fig.axes, parameter_list, active_params)):
            if bounds[num]['tune']:
                x_min, x_max = min(dataframe[parameter].min(), vs.default_bounds[num]['hardlb'], bounds[num]['hardlb']), max(dataframe[parameter].max(), vs.default_bounds[num]['hardub'], bounds[num]['hardub'])
                ax.axvspan(x_min, bounds[num]['hardlb'], alpha=0.3, facecolor='red')
                ax.axvspan(bounds[num]['hardub'], x_max, alpha=0.3, facecolor='red')
                ax.axvspan(bounds[num]['softlb'], bounds[num]['softub'], alpha=0.3, facecolor='green')
                ax.axvspan(bounds[num]['hardlb'], bounds[num]['softlb'], alpha=0.3, facecolor='yellow')
                ax.axvspan(bounds[num]['softub'], bounds[num]['hardub'], alpha=0.3, facecolor='yellow')
                x_lims = ax.get_xlim()
                ax.set_xlim(min(x_min, x_lims[0]), max(x_max, x_lims[1]))
            else:
                ax.text(.5, .5, 'Tuning Disabled \n ({:.5f})'.format(bounds[num]['default'][0]),
                        horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)
                ax.axvline(bounds[num]['default'][0], linewidth=1.5, color=scalar_map.to_rgba(values[0]))
                ax.set_xlim([np.min(bounds[num]['hardlb']), np.max(bounds[num]['hardub'])])

    plt.show(block=False)

    return fig


def single_parameter_plot(pr, bounds, dataframe=None, fig=None):
    control = {}
    control['histogram_plot'] = True
    control['sr_plot'] = True
    control['vlines_plot'] = False
    vlines = 10
    data_points = 200
    bins = 25

    parameter_list = get_parameters(dataframe)
    if type(pr) == str:
        parameter_number = parameter_list.index(pr)
        parameter_name = pr
    elif type(pr) == int:
        parameter_number = pr
        parameter_name = parameter_list[parameter_number]
    else:
        raise TypeError

    data_sorted = sort_values(dataframe, ['LowestUnsatConstr', 'LowestObj', 'BestSimTime'])

    scalar_map, values = get_color_map(vlines)

    # Create Fig Handle #
    if fig is None:
        fig = plt.figure(1, figsize=(3.5, 2.5))
    # Clear old axes #
    for num, ax in enumerate(fig.axes):
        ax.cla()
        ax.remove()

    fig.add_subplot(1, 1, 1)
    for ax in fig.axes:
        ax.set_title(parameter_name)
        ax.set_ylim([-0.05, 1])

    # Plot vlines #
    # if control['vlines_plot']:
    #     for line in range(vlines):
    #         ax.axvline(data_sorted[parameter_name][line], linewidth=1.5, color=scalar_map.to_rgba(values[line]))

    # SR Plot #
    oc_data, bin_edges = data2bin(dataframe, bins)
    bin_list = {k: [(v[x] + v[x + 1]) / 2 for x in range(len(v) - 1)] for (k, v) in bin_edges.items()}

    ax2 = ax.twinx()
    ax.plot(bin_list[parameter_number], oc_data[parameter_number][:, 0], 'bo-', markersize=2)
    if all(np.isinf(oc_data[parameter_number][:, 0])):
        ymax = 1
    else:
        ymax = np.max(oc_data[parameter_number][:, 0][oc_data[parameter_number][:, 0] != np.Inf]) + 1
    ymin = -ymax / 20

    ax.set_xlim(auto=True)
    try:
        ax.set_ylim([ymin, ymax])
    except ValueError:
        pass
    ax.set_ylabel('Constraints')
    ax.set_title(parameter_list[parameter_number])
    ax.yaxis.label.set_color('b')
    ax.tick_params(axis='y', colors='b')
    ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))

    ax2.plot(bin_list[parameter_number], oc_data[parameter_number][:, 1], 'C1o-', markersize=2)

    if all(np.isinf(oc_data[parameter_number][:, 1])):
        ymax = 1
        ymin = -ymax / 20
    else:
        ymax = np.max(oc_data[parameter_number][:, 1][oc_data[parameter_number][:, 1] != np.Inf]) + 1
        ymin = np.min(oc_data[parameter_number][:, 1]) - 1

    try:
        ax2.set_ylim([ymin, ymax])
    except ValueError:
        pass
    ax2.set_ylabel('Objective')
    ax2.set_title(parameter_list[parameter_number])
    ax2.yaxis.label.set_color('C1')
    ax2.tick_params(axis='y', colors='C1')
    ax2.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))

    # Histogram Plot #
    hist_data_points = data_points

    ylims = ax.get_ylim()
    counts, hist_bins = np.histogram(data_sorted[parameter_name][:hist_data_points], bins=bins)
    counts = (counts / np.max(counts)) * ylims[1] * .7
    ax.hist(hist_bins[:-1], hist_bins, weights=counts, bottom=ylims[0], color='blue')
    ax.set_title(parameter_list[parameter_number])

    # Plot Bounds #
    x_min, x_max = dataframe[parameter_name].min(), dataframe[parameter_name].max()
    ax.axvspan(x_min, bounds['hardlb'][parameter_number], alpha=0.3, facecolor='red')
    ax.axvspan(bounds['hardub'][parameter_number], x_max, alpha=0.3, facecolor='red')
    ax.axvspan(bounds['softlb'][parameter_number], bounds['softub'][parameter_number], alpha=0.3, facecolor='green')
    ax.axvspan(bounds['hardlb'][parameter_number], bounds['softlb'][parameter_number], alpha=0.3, facecolor='yellow')
    ax.axvspan(bounds['softub'][parameter_number], bounds['hardub'][parameter_number], alpha=0.3, facecolor='yellow')
    plt.tight_layout()
    return fig


def iteration_plot(dataframe, control=None, fig=None):
    if control is None:
        control ={}
        control['instantaneous'] = True
        control['x_axis'] = 'Iter'
        control['chain'] = 'all'
        control['y_scale_r'] = 'linear'
        control['y_scale_l'] = 'linear'
        control['shift_min'] = False
        control['_unsat'] = 'Constraints'
        # control['x_axis'] = 'TotalWallTime'
        # control['x_axis'] = 'TotalSimTime'
    if control['_unsat'] == 'Constraints':
        control['unsat'] = 'LowestUnsatConstr'
    else:
        control['unsat'] = 'LowestUnsatDist'


    if fig is None:
        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()

    if control['shift_min']:
        objective_shift = dataframe['LowestObj'].min()
    else:
        objective_shift = 0

    fig.axes[0].cla()
    fig.axes[1].cla()
    if control['instantaneous']:
        title_str = "Instant"
        if control['chain'] == 'all':
            chains_iter = dataframe.MCchain.unique()
        else:
            chains_iter = [control['chain']]
        for chain in chains_iter:
            if control['x_axis'] == 'Iter' or control['x_axis'] == 'MCSimTime':
                fig.axes[0].plot(dataframe.loc[dataframe['MCchain'] == chain][control['x_axis']], dataframe.loc[dataframe['MCchain'] == chain][control['unsat']], 'b.')
                fig.axes[1].plot(dataframe.loc[dataframe['MCchain'] == chain][control['x_axis']], dataframe.loc[dataframe['MCchain'] == chain]['LowestObj'] - objective_shift, 'C1.')
            else:
                fig.axes[0].plot(dataframe.loc[dataframe['MCchain'] == chain][control['x_axis']].replace([np.inf, -np.inf], 0).cumsum(), dataframe.loc[dataframe['MCchain'] == chain][control['unsat']], 'b.')
                fig.axes[1].plot(dataframe.loc[dataframe['MCchain'] == chain][control['x_axis']].replace([np.inf, -np.inf], 0).cumsum(), dataframe.loc[dataframe['MCchain'] == chain]['LowestObj'] - objective_shift, 'C1.')
    else:
        title_str = "Best"
        if control['chain'] == 'all':
            chains_iter = dataframe.MCchain.unique()
        else:
            chains_iter = [control['chain']]
        for chain in chains_iter:
            if control['x_axis'] == 'Iter' or control['x_axis'] == 'MCSimTime':
                fig.axes[0].plot(dataframe.loc[dataframe['MCchain'] == chain][control['x_axis']], dataframe.loc[dataframe['MCchain'] == chain][control['unsat']].cummin(), 'b-')
                fig.axes[1].plot(dataframe.loc[dataframe['MCchain'] == chain][control['x_axis']], dataframe.loc[dataframe['MCchain'] == chain]['LowestObj'].cummin() - objective_shift, 'C1-')
            else:
                fig.axes[0].plot(dataframe.loc[dataframe['MCchain'] == chain][control['x_axis']].replace([np.inf, -np.inf], 0).cumsum(), dataframe.loc[dataframe['MCchain'] == chain][control['unsat']].cummin(), 'b-')
                fig.axes[1].plot(dataframe.loc[dataframe['MCchain'] == chain][control['x_axis']].replace([np.inf, -np.inf], 0).cumsum(), dataframe.loc[dataframe['MCchain'] == chain]['LowestObj'].cummin() - objective_shift, 'C1-')

    x_label = 'Iteration'
    if control['x_axis'] == 'Iter':
        x_label = 'Iteration'
    elif control['x_axis'] == 'MCSimTime':
        x_label = 'MCSimTime'
    elif control['x_axis'] == 'TotalWallTime':
        x_label = 'Total Wall Time'
    elif control['x_axis'] == 'TotalSimTime':
        x_label = 'Total Sim Time'
    fig.axes[0].set_xlabel(x_label)
    if control['_unsat'] == 'Constraints':
        fig.axes[0].set_title('Objective/Constraints ('+title_str+') vs ' + x_label, y=1.04)
        fig.axes[0].set_ylabel('Constraints')
    else:
        fig.axes[0].set_title('Objective/Distance (' + title_str + ') vs ' + x_label, y=1.04)
        fig.axes[0].set_ylabel('Distance')

    fig.axes[1].set_ylabel('Objective')
    fig.axes[0].yaxis.label.set_color('b')
    fig.axes[0].tick_params(axis='y', colors='b')
    fig.axes[1].yaxis.label.set_color('C1')
    fig.axes[1].tick_params(axis='y', colors='C1')
    fig.axes[0].ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    fig.axes[1].ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    if control['y_scale_l'] == 'symlog':
        fig.axes[0].set_yscale('symlog', subs=[1, 2, 3, 4, 5, 6, 7, 8, 9])
    else:
        fig.axes[0].set_yscale('linear')
    if control['y_scale_r'] == 'symlog':
        fig.axes[1].set_yscale('symlog', subs=[1, 2, 3, 4, 5, 6, 7, 8, 9])
    else:
        fig.axes[1].set_yscale('linear')
        # Test if data spans an order of magnitude
        # if ymin > 0 and ymax > 0:
        #     min_log = math.ceil(math.log10(ymin))
        #     max_log = math.floor(math.log10(ymax))
        #     if min_log >= max_log:
        #         ymax = 10 ** (max_log + 1)
        #         ymin = 10 ** (min_log - 1)
    plt.show(block=False)

    return fig
