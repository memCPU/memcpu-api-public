import json
import os
import re
import tkinter as tk
from zipfile import ZipFile
import PySimpleGUI as sg
import numpy as np
import pandas as pd
from memAPI import versions as vs
import copy


def traverse_nested_dict(dictionary):
	for key, value in dictionary.items():
		if type(value) is dict:
			yield from traverse_nested_dict(value)
		else:
			yield (key, value)


def update_nested_dict(dictionary, kwargs):
	for key, value in dictionary.items():
		if isinstance(value, dict):
			update_nested_dict(dictionary[key], kwargs)
		elif key in kwargs:
			dictionary[key] = kwargs[key]


def type_coercion(var, ty: type) -> bool:
	try:
		_ = ty(var)
	except ValueError:
		return False
	else:
		return True


def get_history_set_and_parameters(file_path):
	try:
		with open(file_path) as f:
			line = 0
			lines = f.readlines()
			while True:
				line += 1
				try:
					set_number = int(re.search(r'##Set\s([\d]*)\s', lines[line]).group(1))
					parameter_set = list(map(float, re.search(r'##Set\s[\d]*\s:\s(.*)\s', lines[line]).group(1).split()))
					return set_number, parameter_set
				except AttributeError:
					continue
	except FileNotFoundError:
		return None, None


def get_history(file_path):
	if not file_path:
		return None, None
	try:
		with open(file_path) as f:
			first_line = f.readline()
	except FileNotFoundError:
		return None, None

	# First Line #
	try:
		# Get Alpha or Beta
		release = re.search(r'version:\s([a-zA-Z]*)', first_line).group(1)
		if release == "Alpha":
			version = ('Alpha', '0', '1')
		elif release == "Beta":
			version = re.search(r'version:\s([a-zA-Z]*)-([\d])-([\d])-([a-zA-Z]*)', first_line).group(1, 2, 3)
		else:
			version = ('Alpha', '0', '0')
	except AttributeError:
		version = ('Alpha', '0', '0')

	return file_path, version


def get_bounds_json():
	try:
		file_path = tk.filedialog.askopenfilename(
			filetypes=[("bounds.json files", "*.json")])
		with open(file_path) as json_file:
			bounds = json.load(json_file)
		sg.Popup('Success:', 'Boundary file successfully imported')
		return bounds
	except FileNotFoundError:
		sg.Popup('Error:', 'No Boundary file imported')


def create_dataframe(file_path, version, standalone=False):
	if not standalone:
		if version == ('Alpha', '0', '0'):
			# Create Column Header for pre-alpha version
			header = ['Iter', 'MCchain']
			header += ['TimePar' + str(i + 1) for i in range(0, 7)]
			header += ['StaticPar' + str(i + 8) for i in range(0, 10)]
			header += ['TunePos' + str(i + 1) for i in range(0, 17)]
			header += ['LowestUnsatConstr', 'LowestObj', 'BestSimTime', 'Acceptance']

			dataframe = pd.read_csv(file_path, sep=" ", header=None, skiprows=1, float_precision='high')
			dataframe.columns = header
		elif version == ('Alpha', '0', '1'):
			dataframe = pd.read_csv(file_path, sep=" ", header=0, skiprows=1, float_precision='high')
		elif version == ('Beta', '1', '1'):
			dataframe = pd.read_csv(file_path, sep=" ", header=0, skiprows=1, float_precision='high')
		elif version == ('Beta', '2', '1'):
			dataframe = pd.read_csv(file_path, sep=" ", header=0, comment='#', float_precision='high')
		else:
			dataframe = pd.read_csv(file_path, sep=" ", header=0, comment='#', float_precision='high')
	else:
		if version[0] == 'Alpha':
			if version[2] == '0':
				# Create Column Header for pre-alpha version
				header = ['SimTime', 'LowestUnsatConstr', 'LowestObj', 'WallTime']

				dataframe = pd.read_csv(file_path, sep=" ", header=None, skiprows=1, float_precision='high')
				dataframe.columns = header
			elif version[2] == '1':
				dataframe = pd.read_csv(file_path, sep=" ", header=0, skiprows=1, float_precision='high')
		elif version[0] == 'Beta':
			# Beta Version has parset line that must also be skipped
			dataframe = pd.read_csv(file_path, sep=" ", header=0, comment='#', float_precision='high')
		else:
			dataframe = pd.read_csv(file_path, sep=" ", header=0, comment='#', float_precision='high')

	# Add metadata to dataframe
	dataframe.file_name = os.path.basename(file_path)
	dataframe._metadata += ['file_name']
	return dataframe


def create_partable(dataframe, numsets):
	if not isinstance(numsets, list):
		numsets = list(range(numsets))
	data_sorted = sort_values(dataframe, ['LowestUnsatDist', 'LowestObj', 'MCSimTime'])
	parameter_list = get_parameters(dataframe)
	partable = {
		'numpar': len(parameter_list),
		'numset': len(numsets),
		'parset': {}
	}
	for numset in numsets:
		partable['parset'][str(numset)] = data_sorted.loc[numset, parameter_list].to_list()

	with open('partable.json', 'w') as f:
		json.dump(partable, f, indent=4)
	sg.Popup('Complete:', 'Partable.json has been created in working directory.')


def clean_dataframe(dataframe):
	# TODO Deprecate this function
	# drop tuning columns
	dataframe.drop(list(dataframe.filter(regex='TunePos')), axis=1, inplace=True)

	# drop Iter, MCChain and Acceptance
	dataframe.drop(['Iter', 'MCchain', 'Acceptance'], axis=1, inplace=True)

	srt_dict = {'LowestUnsatDist': 'S', 'LowestObj': 'R', 'MCSimTime': 'T'}

	dataframe.rename(columns=srt_dict, inplace=True)
	sort_values(dataframe, by=['S', 'R', 'T'])
	dataframe.reset_index(drop=True, inplace=True)

	return dataframe


def standalone_dataframe_list(file_paths, versions):
	if versions:
		if versions[0][0] == 'Beta':
			tot_set_num = 0
			for file, version in zip(file_paths, versions):
				set_num, _ = get_history_set_and_parameters(file)
				tot_set_num = max(set_num, tot_set_num)
			temp_list = [[] for i in range(tot_set_num + 1)]
			for file, version in zip(file_paths, versions):
				set_num, _ = get_history_set_and_parameters(file)
				temp_list[set_num].append(create_dataframe(file, version, standalone=True))

		else:
			temp_list = []
			if file_paths is not None or file_paths:
				for file, version in zip(file_paths, versions):
					temp_list.append(create_dataframe(file, version, standalone=True))
		return temp_list


def get_credentials():
	if os.path.isfile('settings.json'):
		with open('settings.json') as json_file:
			settings = json.load(json_file)
	else:
		settings = {
			"username": "",
			"password": "",
			"proxy": {
				"http": "",
				"https": ""
			}
		}

	return settings


def unzip_file(file, directory):
	file_path = os.path.join(directory, file)
	if '.zip' in file:
		with ZipFile(file_path, 'r') as zipObj:
			# Extract all the contents of zip file in different directory
			zipObj.extractall(directory)
		os.remove(file_path)


def check_bounds(bounds):
	bool_list = []
	for par in range(len(bounds)):
		bool_list.append(
			bounds[par]['hardlb'] <= bounds[par]['softlb'] <= bounds[par]['softub'] <= bounds[par]['hardub'])

	if not all(bool_list):
		sg.Popup('Warning:', 'Bounds are not properly set')
		return False
	else:
		return True


def sort_values(dataframe, by):
	sorted_data = dataframe.sort_values(by=by)
	sorted_data.reset_index(drop=True, inplace=True)
	return sorted_data


def get_new_bounds(dataframe, soft=10, hard=100, offset=(0, 0), use_soft=True, use_hard=True):
	active_params = get_active(dataframe)
	parameter_list = get_parameters(dataframe)

	sorted_data = sort_values(dataframe, ['LowestUnsatDist', 'LowestObj', 'MCSimTime'])
	data_points = len(sorted_data)
	soft_points = soft
	hard_points = hard

	zeros = [0 for _ in active_params]
	ones = [1 for _ in active_params]

	bounds = copy.deepcopy(vs.default_bounds)

	for num, param in enumerate(parameter_list):
		if active_params[num]:
			bounds[num]['tune'] = 1
			# Can activate hard and soft bounds, can have hard or soft bounds. If no bounds, dont use!
			# Check if should use bounds

			if not soft_points:
				use_soft = False
			if not hard_points:
				use_hard = False

			# Set Soft Bounds
			if use_soft:
				if isinstance(soft_points, list):
					bounds[num]['softlb'] = np.min(sorted_data.iloc[soft_points][param])
					bounds[num]['softub'] = np.max(sorted_data.iloc[soft_points][param])
				else:
					bounds[num]['softlb'] = np.min(sorted_data[0:soft_points][param])
					bounds[num]['softub'] = np.max(sorted_data[0:soft_points][param])

			# Set Hard Bounds, as per Soft Bounds
			if use_hard:
				if isinstance(hard_points, list):
					bounds[num]['hardlb'] = np.min(sorted_data.iloc[hard_points][param])
					bounds[num]['hardub'] = np.max(sorted_data.iloc[hard_points][param])
				else:
					bounds[num]['hardlb'] = np.min(sorted_data[0:hard_points][param])
					bounds[num]['hardub'] = np.max(sorted_data[0:hard_points][param])

			# Check for Consistency
			if not use_soft:
				bounds[num]['softlb'] = bounds[num]['hardlb']
				bounds[num]['softub'] = bounds[num]['hardub']

			bounds[num]['hardlb'] = min(bounds[num]['softlb'], bounds[num]['hardlb'])
			bounds[num]['hardub'] = max(bounds[num]['softub'], bounds[num]['hardub'])

			# Bounds Offset
			if offset:  # Will be zero if no offset
				bounds[num]['softlb'] -= offset[0]
				bounds[num]['softub'] += offset[0]
				bounds[num]['hardlb'] = min(bounds[num]['softlb'] - offset[1], bounds[num]['hardlb'])
				bounds[num]['hardub'] = max(bounds[num]['softub'] + offset[1], bounds[num]['hardub'])

		else:
			bounds[num]['tune'] = 0
	if check_bounds(bounds):
		return bounds
	else:
		sg.Popup('Warning:', 'Bounds are not properly set', non_blocking=False)
		return copy.deepcopy(vs.default_bounds)


def get_active(dataframe):
	parameter_list = get_parameters(dataframe)

	active_params = [False for _ in range(len(parameter_list))]

	for param, column in enumerate(parameter_list):
		if dataframe[column].nunique() != 1:
			active_params[param] = True

	return active_params


def get_parameters(dataframe):
	column_list = dataframe.columns.tolist()
	parameter_list = []
	for label in column_list:
		if re.match("\w+Par\d", label):
			parameter_list.append(label)

	return parameter_list
