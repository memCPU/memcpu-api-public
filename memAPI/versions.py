import math
from memAPI import misc as ms
from memAPI import typedvar as ty
import copy

################################################
"""
First number controls config Version/Solver version. Second is specific build of UI
"""

build_version = 2.06

################################################

api_version = 'Beta Version ' + str(build_version)
config_version = math.floor(build_version)
config_version_settings = {
	'version': config_version,
	'commonsetting': {
		'partablesize': 19
	}
}

################################################

global_defaults = {
	'repeats': 8
}

global_settings = {
	# Main Settings
	'name': '',
	'timeout': None,
	# Common Settings
	'assignfile': "_assignment",
	'plotfile': "_history",
	'stdout': "_stdout",
	'mpsfile': '',
	'defaultsize': [],
	'targetobjective': "-inf",
	'warmstart': "",

	# Advanced Settings
	'unaryresbase': 1,
	'maxexpansionfactor': 1e4,

	# Standalone Settings
	'stride': 50,

	# Monte Carlo Settings
	'iters': 1,
	'mchains': 2,
	'switchfraction': 1e-4,
	'sigmamin': 0.01,
	'sigmamax': 0.25,
	'adaptobjgap': 0,
	'adaptswitchfraction': 1,
	'itersdelaythermalx': 5,
	'nsetsfrompartabledss': 4,

	# NN Settings
	'numlayers': 3,
	'layersize': [200, 10, 1],
	'layertype': ["relu", "sigmoid", "linear"],
	'learningrate': 0.001,
	'numbatch': 1,
	'tuningepoch': 1,
	'trainingepoch': 100,
	'dev': 0.2,
	'dropout1': 0.0,
	'dropout2': 0.2

}

nn_layers = ["sigmoid", "tanh", "relu", "leaky_relu", "linear"]

mode_settings = {
	0: {
		'mode': 0,
		'numcores': 36,
		'simtime': "8",
		'cputime': 3600,
		'initialobjective': 'inf',
		'feasibilitytolerance': 1e-6,
		'replicas': 2
	},
	1: {
		'mode': 1,
		'numcores': 36,
		'simtime': "12",
		'cputime': 300,
		'initialobjective': 'inf',
		'feasibilitytolerance': 1e-6,
		'sigmamin': 0.005,
		'sigmamax': 0.35,
		'iters': 1000,
		'histlen': 1000,
		'mchains': 36,
		'replicas': 2
	},
	2: {
		'mode': 2,
		'timeout': 3600,
		'numcores': 36,
		'simtime': "12",
		'cputime': 300,
		'initialobjective': 'inf',
		'feasibilitytolerance': 1e-6,
		'switchfraction': 0.0001,
		'sigmamin': 0.01,
		'sigmamax': 0.25,
		'iters': 50,
		'histlen': 50,
		'mchains': 36,
		'replicas': 2
	},
	3: {
		'mode': 3,
		'timeout': None,
		'numcores': 36,
		'simtime': "4",
		'cputime': 3600.0,
		'initialobjective': 'inf',
		'feasibilitytolerance': 1e-6,
		'switchfraction': 0.02,
		'sigmamin': 0.01,
		'sigmamax': 0.25,
		'iters': 50,
		'histlen': 50,
		'mchains': 36,
		'replicas': 2,
		'numlayers': 3,
		'layersize': [200, 10,  1],
		'layertype': ["relu", "sigmoid", "linear"],
		'learningrate': 0.001,
		'numbatch': 1,
		'tuningepoch': 1,
		'trainingepoch': 100,
		'dev': 0.2,
		'dropout1': 0.0,
		'dropout2': 0.2
	}
}

instance_settings = {
	# CPU Defaults
	1: {
		'instancetype': 1

	},
	# GPU Defaults
	2: {
		'instancetype': 2,
		'numcores': 8,
		'mchains': 8
	}
}

# Smart Defaults take the current config to determine settings


default_parameters = {
	'tune': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	'hardlb': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	'softlb': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	'softub': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	'hardub': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	'default': [0.684781, 0.21072, 0.325552, 0.212938, 0.755275, 0.79376, 0.640866, 0.07921, 0.892202, 0.580256,
	            0.959262, 0.153888, 0.429571, 0.505088, 0.525961, 0.52735, 0.910827, 0.357494, 0.31304]
}

default_bounds_ = {
	'tune': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	'hardlb': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	'softlb': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	'softub': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
	'hardub': [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
}

default_bounds = [
        {
            "default": [
                0.176766
            ],
            "hardlb":-0.1,
            "hardub":1.25,
            "softlb":-0.1,
            "softub":1.25,
            "tune": 1
        },
        {
            "default": [
                0.122347
            ],
            "hardlb":-0.5,
            "hardub":1.0,
            "softlb":-0.5,
            "softub":1.0,
            "tune": 1
        },
        {
            "default": [
                0.13636
            ],
            "hardlb":-0.5,
            "hardub":1.25,
            "softlb":-0.5,
            "softub":1.25,
            "tune": 1
        },
        {
            "default": [
                0.3859
            ],
            "hardlb":0,
            "hardub":1.5,
            "softlb":0,
            "softub":1.5,
            "tune": 1
        },
        {
            "default": [
                0.433886
            ],
            "hardlb":-0.5,
            "hardub":1.5,
            "softlb":-0.5,
            "softub":1.5,
            "tune": 1
        },
        {
            "default": [
                0.527346
            ],
            "hardlb":-0.2,
            "hardub":1.3,
            "softlb":-0.2,
            "softub":1.3,
            "tune": 1
        },
        {
            "default": [
               0.834247
            ],
            "hardlb":-0.2,
            "hardub":1.3,
            "softlb":-0.2,
            "softub":1.3,
            "tune": 1
        },
        {
            "default": [
               0.475881
            ],
            "hardlb":-0.5,
            "hardub":1.2,
            "softlb":-0.5,
            "softub":1.2,
            "tune": 1
        },
        {
            "default": [
                -0.082216
            ],
            "hardlb":-0.5,
            "hardub":1.5,
            "softlb":-0.5,
            "softub":1.5,
            "tune": 1
        },
        {
            "default": [
               1.439175
            ],
            "hardlb":-0.25,
            "hardub":2.0,
            "softlb":-0.25,
            "softub":2.0,
            "tune": 1
        },
        {
            "default": [
               0.802817
            ],
            "hardlb":-0.5,
            "hardub":2.0,
            "softlb":-0.5,
            "softub":2.0,
            "tune": 1
        },
        {
            "default": [
               0.764153
            ],
            "hardlb":-0.2,
            "hardub":1.25,
            "softlb":-0.2,
            "softub":1.25,
            "tune": 1
        },
        {
            "default": [
               0.625172
            ],
            "hardlb":-0.2,
            "hardub":1.25,
            "softlb":-0.2,
            "softub":1.25,
            "tune": 1
        },
        {
            "default": [
               0.923329
            ],
            "hardlb":0,
            "hardub":1.75,
            "softlb":0,
            "softub":1.75,
            "tune": 1
        },
        {
            "default": [
               0.915244
            ],
            "hardlb":-0.25,
            "hardub":1.25,
            "softlb":-0.25,
            "softub":1.25,
            "tune": 1
        },
        {
            "default": [
               0.657232
            ],
            "hardlb":0,
            "hardub":1.5,
            "softlb":0,
            "softub":1.5,
            "tune": 1
        },
        {
            "default": [
               0.821484
            ],
            "hardlb":0,
            "hardub":1.5,
            "softlb":0,
            "softub":1.5,
            "tune": 1
        },
        {
            "default": [
               0.921739
            ],
            "hardlb":-0.1,
            "hardub":1.1,
            "softlb":-0.1,
            "softub":1.1,
            "tune": 1
        },
        {
            "default": [
               0.405624
            ],
            "hardlb":-0.5,
            "hardub":1.5,
            "softlb":-0.5,
            "softub":1.5,
            "tune": 1
        }
    ]


class ImmutableDict(dict):
	def __init__(self, kwargs):
		dict.__init__(self, **kwargs)

	def __delitem__(self, key):
		raise AttributeError("The key {} cannot be removed.".format(key))

	def __getitem__(self, key):
		if key not in self:
			raise KeyError("The key {} is not defined.".format(key))
		return dict.__getitem__(self, key)

	def __setitem__(self, key, value):
		if key not in self:
			raise KeyError("The key {} is not defined.".format(key))
		dict.__setitem__(self, key, value)


class Validator:
	supported_versions = [1, 2]

	def __init__(self, version: int, mode: int, instance: int):
		assert version in Validator.supported_versions, \
			f"Requested Config Version ({version: version}) is not Supported"
		self.version = version
		self.mode = mode
		self.instance = instance

	def get_config(self) -> dict:
		"""
		:return: Structure of the current version of the config, empty
		:rtype:
		"""
		partable = [
			ImmutableDict({k: ([] if k == 'default' else v[i]) for k, v in default_parameters.items()})
			for i in range(19)
		]
		if self.version == 1:
			config = {
				'instancetype': None,
				'name': None,
				'version': 1,
				'timeout': None,
				'commonsetting': {
					'mode': None,
					'numcores': None,
					'mpsfile': None,
					'assignfile': None,
					'plotfile': None,
					'stdout': None,
					'simtime': None,
					'cputime': None,
					'replicas': None,
					'initialobjective': None,
					'feasibilitytolerance': None,
					'partablesize': 19,
					'defaultsize': None
				},
				'standalonesetting': {
					'targetobjective': None,
					'warmstart': None
				},
				'mcsetting': {
					'iters': None,
					'mchains': None,
					'switchfraction': None,
					'sigmamin': None,
					'sigmamax': None
				},
				'partable': partable
			}
		elif self.version == 2:
			config = {
				'instancetype': None,
				'name': None,
				'version': 2,
				'timeout': None,
				'commonsetting': {
					'mode': None,
					'numcores': None,
					'mpsfile': None,
					'assignfile': None,
					'plotfile': None,
					'stdout': None,
					'simtime': None,
					'cputime': None,
					'replicas': None,
					'initialobjective': None,
					'targetobjective': None,
					'feasibilitytolerance': None,
					'partablesize': 19,
					'defaultsize': None,
					'warmstart': None,
					'fixedseed': None,
					'unaryresbase': None,
					'maxexpansionfactor': None

				},
				'standalonesetting': {
					'stride': None
				},
				'mcsetting': {
					'iters': None,
					'mchains': None,
					'switchfraction': None,
					'sigmamin': None,
					'sigmamax': None,
					'histlen': None,
					'adaptobjgap': None,
					'adaptswitchfraction': None,
					'itersdelaythermalx': None,
					'nsetsfrompartabledss': None
				},
				'nnsetting': {
					'numlayers': None,
					'layersize': None,
					'layertype': None,
					'learningrate': None,
					'numbatch': None,
					'tuningepoch': None,
					'trainingepoch': None,
					'dev': None,
					'dropout1': None,
					'dropout2': None
				},
				'partable': partable
			}
		else:
			config = {}

		# Update Global Defaults
		ms.update_nested_dict(config, copy.deepcopy(global_settings))

		# Update Mode Defaults
		ms.update_nested_dict(config, copy.deepcopy(mode_settings[self.mode]))

		# Update Instance Defaults
		ms.update_nested_dict(config, copy.deepcopy(instance_settings[self.instance]))

		return config

	def get_types(self) -> dict:
		"""

		:return: The type of each entry, unstructured
		:rtype:
		"""
		types = {}
		if self.version == 1:
			types = {
				'instancetype': ty.IntBounded('instancetype', bounds=[1, 2]),
				'name': ty.Str('name'),
				'version': ty.Int('version'),
				'timeout': ty.Int('timeout'),
				'mode': ty.IntBounded('mode', bounds=[0, 2]),
				'numcores': ty.IntBounded('numcores', bounds=[1, None]),
				'mpsfile': ty.Str('mpsfile'),
				'assignfile': ty.Str('assignfile'),
				'plotfile': ty.Str('plotfile'),
				'stdout': ty.Str('stdout'),
				'simtime': ty.StrFunctioned('simtime', func=lambda inst, val: ms.type_coercion(val, float)),
				'cputime': ty.Float('cputime'),
				'replicas': ty.Int('replicas'),
				'initialobjective': ty.StrFunctioned('initialobjective',
				                                     func=lambda inst, val: ms.type_coercion(val, float)),
				'targetobjective': ty.StrFunctioned('targetobjective',
				                                    func=lambda inst, val: ms.type_coercion(val, float)),
				'feasibilitytolerance': ty.Float('feasibilitytolerance'),
				'partablesize': ty.Int('partablesize'),
				'warmstart': ty.StrFunctioned('warmstart',
				                              func=lambda inst, val: True if val in ['', 'warmstart.json'] else False),
				'defaultsize': ty.List('defaultsize'),
				'stride': ty.IntBounded('stride', bounds=[1, None]),
				'iters': ty.IntBounded('iters', bounds=[1, None]),
				'mchains': ty.IntBounded('mchains', bounds=[1, None]),
				'switchfraction': ty.FloatBounded('switchfraction', bounds=[0, 1]),
				'sigmamin': ty.FloatBounded('sigmamin', bounds=[0, 1]),
				'sigmamax': ty.FloatBounded('sigmamax', bounds=[0, 1]),
				'partable': ty.List('partable')
			}
		elif self.version == 2:
			types = {
				'instancetype': ty.IntBounded('instancetype', bounds=[1, 2]),
				'name': ty.Str('name'),
				'version': ty.Int('version'),
				'timeout': ty.Int('timeout'),
				'mode': ty.IntBounded('mode', bounds=[0, 3]),
				'numcores': ty.IntBounded('numcores', bounds=[1, None]),
				'mpsfile': ty.Str('mpsfile'),
				'assignfile': ty.Str('assignfile'),
				'plotfile': ty.Str('plotfile'),
				'stdout': ty.Str('stdout'),
				'simtime': ty.StrFunctioned('simtime', func=lambda inst, val: ms.type_coercion(val, float)),
				'cputime': ty.Float('cputime'),
				'replicas': ty.Int('replicas'),
				'initialobjective': ty.StrFunctioned('initialobjective',
				                                     func=lambda inst, val: ms.type_coercion(val, float)),
				'targetobjective': ty.StrFunctioned('targetobjective',
				                                    func=lambda inst, val: ms.type_coercion(val, float)),
				'feasibilitytolerance': ty.Float('feasibilitytolerance'),
				'partablesize': ty.Int('partablesize'),
				'warmstart': ty.StrFunctioned('warmstart',
				                              func=lambda inst, val: True if val in ['', 'warmstart.json'] else False),
				'defaultsize': ty.List('defaultsize'),
				'stride': ty.IntBounded('stride', bounds=[0, None]),
				'iters': ty.IntBounded('iters', bounds=[1, None]),
				'mchains': ty.IntBounded('mchains', bounds=[1, None]),
				'switchfraction': ty.FloatBounded('switchfraction', bounds=[0, 1]),
				'sigmamin': ty.FloatBounded('sigmamin', bounds=[0, 1]),
				'sigmamax': ty.FloatBounded('sigmamax', bounds=[0, 1]),
				'histlen': ty.Int('histlen'),
				'partable': ty.List('partable'),
				'numlayers': ty.Int('numlayers'),
				'layersize': ty.List('layersize'),
				'layertype': ty.List('layertype'),
				'learningrate': ty.Float('learningrate'),
				'numbatch': ty.Int('numbatch'),
				'tuningepoch': ty.Int('tuningepoch'),
				'trainingepoch': ty.Int('trainingepoch'),
				'dev': ty.Float('dev'),
				'dropout1': ty.FloatBounded('dropout1', bounds=[0, 1]),
				'dropout2': ty.FloatBounded('dropout2', bounds=[0, 1]),
				'fixedseed': ty.Int('fixedseed'),
				'adaptobjgap': ty.IntBounded('adaptobjgap', bounds=[0, 1]),
				'adaptswitchfraction': ty.IntBounded('adaptswitchfraction', bounds=[0, 1]),
				'itersdelaythermalx': ty.IntBounded('itersdelaythermalx', bounds=[0, None]),
				'nsetsfrompartabledss': ty.IntBounded('nsetsfrompartabledss', bounds=[1, None]),
				'unaryresbase': ty.StrFunctioned('unaryresbase',
				                                     func=lambda inst, val: ms.type_coercion(val, float)),
				'maxexpansionfactor': ty.FloatBounded('maxexpansionfactor', bounds=[1, 1e4])
			}
		return types

	def get_validation(self) -> dict:

		if self.version == 1:
			validation = {
				'not_required': 'timeout',
				'validate': {
					'partable': lambda self: len(self.partable) == self.partablesize and
					                         len(set([len(par['default']) for par in self.partable])) <= 1 and
					                         all([len(par['default']) == (
						                         len(self.defaultsize) if self.defaultsize else False) for par in
					                              self.partable])

				}
			}
		elif self.version == 2:
			validation = {
				'not_required': ['timeout', 'fixedseed'],
				'validate': {
					'partable': lambda self: len(self.partable) == self.partablesize and
					                         len(set([len(par['default']) for par in self.partable])) <= 1 and
					                         all([len(par['default']) == (
						                         len(self.defaultsize) if self.defaultsize else False) for par in
					                              self.partable])

				}
			}
		else:
			validation = {
				'not_required': ['timeout', 'fixedseed'],
				'validate': {
					'partable': lambda self: len(self.partable) == self.partablesize and
					                         len(set([len(par['default']) for par in self.partable])) <= 1 and
					                         all([len(par['default']) == (
						                         len(self.defaultsize) if self.defaultsize else False) for par in
					                              self.partable])

				}
			}
		return validation

	def get_smart_defaults(self) -> dict:
		smart_defaults = {}

		if self.version == 1:
			pass
		elif self.version == 2:
			smart_defaults = {
				'histlen': lambda self: self.iters
			}

		return smart_defaults
