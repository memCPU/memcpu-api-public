import requests
import os
import json
import PySimpleGUI as sg


verify_flag = True


class MemSaaS:
	def __init__(self):

		self.email = ''
		self.password = ''

		self.api_key = ''
		self.token = ''

		self.proxy = {
			'http': '',
			'https': ''
		}

		self.default_download_folder = os.path.join(os.getcwd(), "Downloads")

		self.load_settings()

		self.base_memcpu_url = 'https://api.memcpu.io/api/v2'

		self.get_health_url = self.base_memcpu_url + '/health'
		self.get_my_jobs_url = self.base_memcpu_url + '/jobs'
		self.auth_test_url = self.base_memcpu_url + '/token/'
		self.jobs_url = self.base_memcpu_url + '/jobs'
		self.jobs_create_url = self.base_memcpu_url + '/jobs/create/'
		self.update_url = self.base_memcpu_url + '/jobs/'
		self.files_url = self.base_memcpu_url + '/jobs/'
		self.job_mps_url = self.base_memcpu_url + '/jobs/{}/files/input/'
		self.job_warmstart_url = self.base_memcpu_url + '/jobs/{}/files/warm-start/'

	def has_credentials(self):
		if self.email and self.password and self.api_key:
			if self.auth_test():
				return True
		return False

	def save_settings(self):
		settings = {}
		settings['login_credentials'] = {
			"username": self.email,
			"password": self.password,
			"api_key": self.api_key,
			"proxy": self.proxy
		}
		settings['download_dir'] = self.default_download_folder
		with open('settings.json', 'w') as outfile:
			json.dump(settings, outfile, indent=4)

	def load_settings(self):
		if os.path.isfile('settings.json'):
			with open('settings.json') as json_file:
				settings = json.load(json_file)
			if 'login_credentials' in settings:
				self.email = settings['login_credentials']['username']
				self.password = settings['login_credentials']['password']
				if 'api_key' in settings['login_credentials']:
					self.api_key = settings['login_credentials']['api_key']
				self.proxy = {
					'http': settings['login_credentials']['proxy']['http'],
					'https': settings['login_credentials']['proxy']['https']
				}
			if 'download_dir' in settings:
				self.default_download_folder = settings['download_dir']

	def save_default_folder(self):
		settings = {}
		if os.path.isfile('settings.json'):
			with open('settings.json') as json_file:
				settings = json.load(json_file)
		settings['download_dir'] = self.default_download_folder
		with open('settings.json', 'w') as outfile:
				json.dump(settings, outfile, indent=4)

	def get_credentials_ui(self):
		form = sg.FlexForm('Login')  # begin with a blank form

		layout = [
			[sg.Text('SaaS API Login Credentials', font=('default', 14, 'bold'))],
			[sg.Text('MemComputing Inc. ')],
			[sg.Text('_' * 60)],
			[sg.Text('Username:', size=(15, 1)), sg.In(default_text=self.email, key='username')],
			[sg.Text('Password:', size=(15, 1)),
			 sg.In(default_text=self.password, password_char='*', key='password')],
			[sg.Text('API Key:', size=(15, 1)), sg.In(default_text=self.api_key, key='api_key')],
			[sg.Checkbox('Remember Login', key='save_login')],
			[sg.Text('Optional Proxy Server Settings:')],
			[sg.Text('http:', size=(15, 1)), sg.In(default_text=self.proxy['http'], key='http')],
			[sg.Text('https:', size=(15, 1)), sg.In(default_text=self.proxy['https'], key='https')],

			[sg.Submit()]
		]
		form.Layout(layout)
		while True:
			button, credentials = form.Read()
			if button is None:
				break
			if button == 'Submit':
				if not credentials['username']:
					sg.Popup('Missing:', 'Username')
				elif not credentials['password']:
					sg.Popup('Missing:', 'Password')
				elif not credentials['api_key']:
					sg.Popup('Missing:', 'API Key')
				else:
					self.email = credentials['username']
					self.password = credentials['password']
					self.api_key = credentials['api_key']
					self.proxy['http'] = credentials['http']
					self.proxy['https'] = credentials['https']
					if credentials['save_login']:
						self.save_settings()
					if self.auth_test():
						break

		form.Close()
		return

	@property
	def headers(self):
		headers = {
			'Accept': '*/*',
			'Cache-Control': 'no-cache',
			'Connection': 'keep-alive',
			'Api-Key': self.api_key,
			'Authorization': 'Bearer {}'.format(self.token)
		}

		return headers

	def auth_test(self):
		payload = {
			'email': self.email,
			'password': self.password
		}
		r = requests.post(self.auth_test_url, data=payload, proxies=self.proxy)
		if r.status_code == 200:
			response = r.json()
			self.token = response['access']
			return True
		elif r.status_code == 401:
			sg.Popup('Login Failed:', 'Username and/or password is incorrect')
			return False
		else:
			sg.Popup('Login Failed:', 'Error ' + str(r.status_code))
			return False

	def list_my_jobs(self):
		r = requests.get(self.get_my_jobs_url, headers=self.headers, auth=(self.email, self.password),
		                 proxies=self.proxy, verify=verify_flag)

		return r

	def list_files(self, job_id):
		the_url = self.files_url + str(job_id) + '/files'
		r = requests.get(the_url, headers=self.headers, auth=(self.email, self.password),
		                 proxies=self.proxy, verify=verify_flag)

		if r.status_code == 200:
			return r
		elif r.status_code == 403:
			if self.api_key:
				sg.Popup('Login Failed:', 'API key is incorrect')

		else:
			sg.Popup('Login Failed:', 'Error ' + str(r.status_code))

	def get_named_file(self, job_id, filename, file_destination=None):
		the_url = self.files_url + str(job_id) + '/files/' + filename
		if file_destination is None:
			current_dir = os.getcwd()
			full_file = os.path.join(current_dir, filename)
		else:
			if not os.path.isdir(file_destination):
				os.mkdir(file_destination)
			full_file = os.path.join(file_destination, filename)
		r = requests.get(the_url, headers=self.headers, auth=(self.email, self.password),
		                 proxies=self.proxy, verify=verify_flag)

		with open(full_file, 'wb') as file:
			file.write(r.content)

		return r

	def create_job(self, mps_file, job_file, warmstart_file=None, partable_file=None):
		if warmstart_file:
			multipart_form_data = [
				('file', (os.path.basename(mps_file), open(mps_file, 'rb'))),
				('job', (os.path.basename(job_file), open(job_file, 'rb'))),
				('warm_start', (os.path.basename(warmstart_file), open(warmstart_file, 'rb')))
			]
		else:
			multipart_form_data = [
				('file', (os.path.basename(mps_file), open(mps_file, 'rb'))),
				('job', (os.path.basename(job_file), open(job_file, 'rb')))
			]

		r = requests.post(self.jobs_create_url, auth=(self.email, self.password), headers=self.headers, files=multipart_form_data,
		                  proxies=self.proxy, verify=verify_flag)

		if not r.status_code == 201:
			print("Error Creating Job.")
			print(r.status_code)
			print(r.json())

		else:
			print("Job Created.")

			# Get Job ID
			job_id = r.json()['id']

			if partable_file:
				r = self.change_partable(job_id, partable_file)
			if r.status_code == 200:
				print(f"SUCCESS!  Partable Uploaded")
			else:
				print("FAILURE Uploading Partable")

			print("REQUESTING THE JOB TO BE RUN")
			r = requests.patch(self.jobs_url + '/' + str(job_id) + '/status/', headers=self.headers, auth=(self.email, self.password), json={"status": "queued"},
			                   proxies=self.proxy, verify=verify_flag)
			json_results = r.json()
			if r.status_code == 200:
				print(f"SUCCESS!  New Job Status: {json_results}")
			else:
				print("FAILURE Queuing Job")

	def cancel_my_job(self, job_id):
		the_url = self.jobs_url + '/' + str(job_id) + '/status/'
		r = requests.patch(the_url, headers=self.headers, auth=(self.email, self.password), json={"status": "cancelled"},
		                   proxies=self.proxy, verify=verify_flag)
		return r

	def change_partable(self, job_id, partable):
		the_url = self.jobs_url + '/' + str(job_id) + '/files/partable/'
		print(partable)
		files = [
			('partable', open(partable, 'rb'))
		]
		r = requests.put(the_url, headers=self.headers, auth=(self.email, self.password), files=files,
		                   proxies=self.proxy, verify=verify_flag)

		return r
